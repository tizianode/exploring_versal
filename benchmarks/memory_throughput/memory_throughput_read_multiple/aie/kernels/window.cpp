#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "window.h"

/*
  Kernel to measure off chip memory read bandwidth using vectorised window communication
  It sums up all the incoming numbers and sends the sum back
*/
#define VECTOR_LEN 8

void kernel_window(input_window<int> *in, output_window<int> *out)
{
  v8int32 xV = null_v8int32();
  v8int32 zV = null_v8int32();

  const unsigned LSIZE = (NUM_SAMPLES / VECTOR_LEN);
  for (unsigned i = 0; i < LSIZE; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_readincr(in, xV);
      zV = operator+(xV, zV);
    }

  int z = 0;
  for (unsigned i = 0; i < VECTOR_LEN; i++)
  {
    z += ext_elem(zV, i);
  }

  window_writeincr(out, z);
  window_writeincr(out, 0);
}