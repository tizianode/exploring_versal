This is a simple multicast communication benchmark. One sender sends data of specific sizes using streams. If the data to be sent is more than 16KB it triggers the kernel multiple times.

## Configuration

The sample requires Vitis `2022.1`, with the classical configuration typical of Xilinx Versal tutorials.

The `PLATFORM_REPO_PATHS` env variable contains the platform to the Versal base platform (e.g., `/opt/Xilinx/Vitis/2022.1/base_platforms/`).

The Makefile has been adapted from: https://github.com/Xilinx/Vitis-Tutorials/tree/2022.1/AI_Engine_Development/Feature_Tutorials/05-AI-engine-versal-integration


**Note** that by default this is using the DFX base platform. To switch to the base platform, you have to edit the Makefile (comment/uncomment lines 5 and 6).


## Directory structure

This sample is organized as follows:
- the `aie` folder contains the AI-Engine source files (kernels and graph)
- the `sw` folder contains two programs that are neede for sw_ and hw_emulation + the xrt.ini file
- the `Makefile` is taking care of the whole compilation process

## Simulate on the server

It is possible to run the AI Engine code directly on the server, using the `x86simulator`.

```bash
make aie sim
```

Or using the `aiesimulator`.
```bash
make aie sim TARGET=hw
```

If everything runs successfully you will see a print out at the end of the simulation:
```
Result is correct!!!
```

**Note**: in simulation, the amount of data sent is defined in `graph.cpp` (line 62). In particular, you can change the number of iterations.
This program works in simulation if the number of iterations is 1 or 2. It stall if the number of iterations is > 2.

## Building and runfor software emulation
To use software emulation just type:

```bash 
make run TARGET=sw_emu
```

and for hardware emulation:

```bash
make run TARGET=hw_emu
```

**Note**: When using the `run` rule everything will be removed first and then completely recompiled from scratch. If you want to keep certain compiled stages run `make` only for the stages that need to be compiled. Make sure that everything is compiled with the same `TARGET`.

### Running in HW

For compiling the design in Hardware, the flow is pretty much the same, but we need to pass the `TARGET=hw` flag to the compilation stages.

**Note:** the flow will require few minutes to complete


```bash
make run TARGET=hw
```

If you want to skip the `aiesimulator` simulation type:
```bash 
make clean_all aie xsa p2p_app package TARGET=hw
```