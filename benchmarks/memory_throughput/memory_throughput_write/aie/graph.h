#include <adf.h>
#include "kernels/window.h"
#include "kernels/stream.h"
#include "kernels/window_64.h"
#include "kernels/window_128.h"

using namespace adf;

class windowGraphGMIO : public graph
{
private:
    kernel kern_window_gmio;

public:
    output_gmio out_window_gmio;

    windowGraphGMIO()
    {

        printf("Init\n");

        // create gmio
        out_window_gmio = output_gmio::create("out_window_gmio", 64, 1000);
        printf("GMIO created\n");

        kern_window_gmio = kernel::create(kernel_window);

        connect<window<NUM_SAMPLES * sizeof(int)>> net1(kern_window_gmio.out[0], out_window_gmio.in[0]);

        source(kern_window_gmio) = "kernels/window.cpp";

        runtime<ratio>(kern_window_gmio) = 0.1;
    }
};

class streamGraphGMIO : public graph
{
private:
    kernel kern_stream_gmio;

public:
    output_gmio out_stream_gmio;

    streamGraphGMIO()
    {

        printf("Init\n");

        // create gmio
        out_stream_gmio = output_gmio::create("out_stream_gmio", 64, 1000);
        printf("GMIO created\n");

        kern_stream_gmio = kernel::create(kernel_stream);

        connect<stream> net1(kern_stream_gmio.out[0], out_stream_gmio.in[0]);

        source(kern_stream_gmio) = "kernels/stream.cpp";

        runtime<ratio>(kern_stream_gmio) = 0.1;
    }
};

class windowGraphPLIO : public graph
{
private:
    kernel kern_window_plio;

public:
    output_plio out_window_plio;
    windowGraphPLIO()
    {
        printf("Init\n");

        out_window_plio = output_plio::create("out_window_plio", plio_32_bits, "data/outputwindow.txt");
        printf("PLIO created\n");

        kern_window_plio = kernel::create(kernel_window);

        connect<window<NUM_SAMPLES * sizeof(int)>> net1(kern_window_plio.out[0], out_window_plio.in[0]);
        source(kern_window_plio) = "kernels/window.cpp";

        runtime<ratio>(kern_window_plio) = 0.1;
    }
};

class streamGraphPLIO : public graph
{
private:
    kernel kern_stream_plio;

public:
    output_plio out_stream_plio;

    streamGraphPLIO()
    {

        printf("Init\n");

        // create gmio
        out_stream_plio = output_plio::create("out_stream_plio", plio_32_bits, "data/outputstream.txt");
        printf("PLIO created\n");

        kern_stream_plio = kernel::create(kernel_stream);

        connect<stream> net1(kern_stream_plio.out[0], out_stream_plio.in[0]);

        source(kern_stream_plio) = "kernels/stream.cpp";

        runtime<ratio>(kern_stream_plio) = 0.1;
    }
};

// PLIO with 64 bit width
class windowGraphPLIO64 : public graph
{
private:
    kernel kern_window_plio_64;

public:
    output_plio out_window_plio_64;
    windowGraphPLIO64()
    {
        printf("Init\n");

        out_window_plio_64 = output_plio::create("out_window_plio_64", plio_64_bits, "data/outputwindow_64.txt");
        printf("PLIO created\n");

        kern_window_plio_64 = kernel::create(kernel_window_64);

        connect<window<NUM_SAMPLES * sizeof(int)>> net1(kern_window_plio_64.out[0], out_window_plio_64.in[0]);
        source(kern_window_plio_64) = "kernels/window_64.cpp";

        runtime<ratio>(kern_window_plio_64) = 0.1;
    }
};

class streamGraphPLIO64 : public graph
{
private:
    kernel kern_stream_plio_64;

public:
    output_plio out_stream_plio_64;

    streamGraphPLIO64()
    {

        printf("Init\n");

        // create gmio
        out_stream_plio_64 = output_plio::create("out_stream_plio_64", plio_64_bits, "data/outputstream_64.txt");
        printf("PLIO created\n");

        kern_stream_plio_64 = kernel::create(kernel_stream);

        connect<stream> net1(kern_stream_plio_64.out[0], out_stream_plio_64.in[0]);

        source(kern_stream_plio_64) = "kernels/stream.cpp";

        runtime<ratio>(kern_stream_plio_64) = 0.1;
    }
};

// PLIO with 128 bit width
class windowGraphPLIO128 : public graph
{
private:
    kernel kern_window_plio_128;

public:
    output_plio out_window_plio_128;
    windowGraphPLIO128()
    {
        printf("Init\n");

        out_window_plio_128 = output_plio::create("out_window_plio_128", plio_128_bits, "data/outputwindow_128.txt");
        printf("PLIO created\n");

        kern_window_plio_128 = kernel::create(kernel_window_128);

        connect<window<NUM_SAMPLES * sizeof(int)>> net1(kern_window_plio_128.out[0], out_window_plio_128.in[0]);
        source(kern_window_plio_128) = "kernels/window_128.cpp";

        runtime<ratio>(kern_window_plio_128) = 0.1;
    }
};

class streamGraphPLIO128 : public graph
{
private:
    kernel kern_stream_plio_128;

public:
    output_plio out_stream_plio_128;

    streamGraphPLIO128()
    {

        printf("Init\n");

        // create gmio
        out_stream_plio_128 = output_plio::create("out_stream_plio_128", plio_128_bits, "data/outputstream_128.txt");
        printf("PLIO created\n");

        kern_stream_plio_128 = kernel::create(kernel_stream);

        connect<stream> net1(kern_stream_plio_128.out[0], out_stream_plio_128.in[0]);

        source(kern_stream_plio_128) = "kernels/stream.cpp";

        runtime<ratio>(kern_stream_plio_128) = 0.1;
    }
};
