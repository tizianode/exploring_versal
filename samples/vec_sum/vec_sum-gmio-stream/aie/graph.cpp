#include "graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>

/***************************************************************\
 *  This programm is used for x86simulation and aiesimulation.  *
 *  It can be used to check, that:                              *
 *  - the right amount of data is send to the kernels           *
 *  - the right amount of data is send back from the kernels    *
 *  - the returned data checks out with the expected results    *
 *  If this programm is executing perfectly fine without errors  *
 *  It is most likely, that the things listed above are correct.*
 \**************************************************************/

using namespace adf;

simpleGraph mygraph;

int main(int argc, char **argv)
{
  const int numOfTests = 2;
  const int numSamples[numOfTests] = {
      2048,
      4096,
  };

  adf::return_code ret;

  for (int i = 0; i < numOfTests; i++)
  {
    int n = numSamples[i];
    std::cout << "Running Tests for " << n << " Samples" << std::endl;
    int NUM_SAMPLES_TOTAL = n;

    int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;

    int BLOCK_SIZE_in_Bytes = NUM_SAMPLES_TOTAL * sizeof(float);
    int BLOCK_SIZE_out_Bytes = NUM_SAMPLES_TOTAL * sizeof(float);

    printf("Start program\n");
    mygraph.init();
    printf("Starting graph ....\n");

    // GMIO Malloc all needed arrays
    float *xArray = (float *)GMIO::malloc(BLOCK_SIZE_in_Bytes);
    float *yArray = (float *)GMIO::malloc(BLOCK_SIZE_in_Bytes);
    float *zArray = (float *)GMIO::malloc(BLOCK_SIZE_in_Bytes);
    std::cout << "GMIO::malloc completed" << std::endl;
    float *zGolden = new float[NUM_SAMPLES_TOTAL];

    // Init data arrays
    for (int i = 0; i < NUM_SAMPLES_TOTAL; i++)
    {
      xArray[i] = i;
      yArray[i] = i;
      zGolden[i] = i + i;
    }
    int error = 0;

    // connect the gmios of the graph with the arrays
    mygraph.in_x.gm2aie_nb(xArray, BLOCK_SIZE_in_Bytes);
    mygraph.in_y.gm2aie_nb(yArray, BLOCK_SIZE_in_Bytes);

    // actually run the graph
    ret = mygraph.run(ITERATIONS);
    if (ret != adf::ok)
    {
      printf("Run failed\n");
      return ret;
    }
    // get the ouput from the aie
    mygraph.out.aie2gm_nb(zArray, BLOCK_SIZE_in_Bytes);
    mygraph.out.wait();
    mygraph.wait();

    ret = mygraph.end();

    int errorCount = 0;
    // Verification of the results
    for (int j = 0; j < NUM_SAMPLES; j++)
    {
      if (zArray[j] != zGolden[j])
      {
        std::cout << "ERROR:dout[" << j << "]=" << zArray[j] << ",gold=" << zGolden[j] << std::endl;
        errorCount++;
      }
    }
    if (errorCount)
      printf("Test failed with %d errors\n", errorCount);
    else
      printf("TEST PASSED\n");

    // finishing up
    std::cout << "GMIO transactions finished" << std::endl;
    GMIO::free(xArray);
    GMIO::free(yArray);
    GMIO::free(zArray);
    delete[] zGolden;
  }
  if (ret != adf::ok)
  {
    printf("End failed\n");
    return ret;
  }

  return 0;
}
