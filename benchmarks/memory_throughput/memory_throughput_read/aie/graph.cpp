/***************************************************************************\
* This programm is used for x86simulation and aiesimulation.                *
* It can be used to check, that:                                            *
* - the various graph combinations of GMIO/PLIO and windows/streams         *
*   are set up correctly                                                    *
* - they are computing the correct output                                   *
\***************************************************************************/

#include "graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>

// To use GMIO in Hardware flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"
// Used for basic timing
#include <chrono>
#include <ctime>
class Timer
{
    std::chrono::high_resolution_clock::time_point mTimeStart;

public:
    Timer() { reset(); }
    long long stop()
    {
        std::chrono::high_resolution_clock::time_point timeEnd =
            std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                     mTimeStart)
            .count();
    }
    void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};
#endif

using namespace adf;

// This sets up the graphs and hardware for all the different combinations of GMIO and PLIO with windows and streams.
// All of the graphs will be present in the hardware at the same time. In this case this is not a problem because we
// evaluate them one by one and our program is so small that there are enough interfaces and connections for everything.
windowGraphGMIO wGraphGMIO;
streamGraphGMIO sGraphGMIO;
windowGraphPLIO wGraphPLIO;
streamGraphPLIO sGraphPLIO;
windowGraphPLIO64 wGraphPLIO64;
streamGraphPLIO64 sGraphPLIO64;
windowGraphPLIO128 wGraphPLIO128;
streamGraphPLIO128 sGraphPLIO128;

const int numOfTests = 2;
const int numSamples[15] = {
    4096,
    8192,
    16384,
    32768,
    327680,
    524288,
    1048576,
    2097152,
    5242880,
    10485760,
    20971520,
    41943040,
    68157440,
    104857600};

// function specifcally for the benchmarking of GMIO based connections
int benchmark_gmio(input_gmio in_port, output_gmio out_port, graph mygraph)
{
    auto throughputs = new float[2][numOfTests];
    adf::return_code ret;

    // Starting graph
    mygraph.init();

    for (int i = 0; i < numOfTests; i++)
    {
        int n = numSamples[i];
        if (n % NUM_SAMPLES != 0)
        {
            std::cout << n << " is not multiple of" << NUM_SAMPLES << "! " << n % NUM_SAMPLES << std::endl;
            return -1;
        }

        std::cout << "Running Tests for " << n << " Samples" << std::endl;
        int NUM_SAMPLES_TOTAL = n;

        int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;

        int BLOCK_SIZE_in_Bytes = NUM_SAMPLES_TOTAL * sizeof(int);
        int BLOCK_SIZE_out_Bytes = ITERATIONS * 2 * sizeof(int);

        // GMIO Malloc all needed arrays
        int *inArray = (int *)GMIO::malloc(BLOCK_SIZE_in_Bytes);
        int *outArray = (int *)GMIO::malloc(BLOCK_SIZE_out_Bytes);
        int *outGolden = new int[ITERATIONS * 2];

        // Init data arrays
        for (int i = 0; i < NUM_SAMPLES_TOTAL; i++)
        {
            inArray[i] = 1;
        }
        // Initialize refernence data
        for (int i = 0; i < ITERATIONS * 2; i += 2)
        {
            outGolden[i] = NUM_SAMPLES;
            outGolden[i + 1] = 0; // Needed for PLIO streams, done here for consistency (same kernel can be used)
        }

        // run the graph
        ret = mygraph.run(ITERATIONS);
        if (ret != adf::ok)
        {
            printf("Run failed\n");
            return -1;
        }

        // connect the gmio of the graph with the arrays
        in_port.gm2aie_nb(inArray, BLOCK_SIZE_in_Bytes);

        // get the ouput from the aie
        out_port.aie2gm_nb(outArray, BLOCK_SIZE_out_Bytes);
        out_port.wait();

        // wait for the graph to finish execution
        mygraph.wait();

        // Verification of the results
        int errorCount = 0;
        for (int j = 0; j < ITERATIONS * 2; j++)
        {
            if (outArray[j] != outGolden[j])
            {
                std::cout << "ERROR:dout[" << j << "]=" << outArray[j] << ",gold=" << outGolden[j] << std::endl;
                errorCount++;
            }
        }
        if (errorCount)
            printf("Test failed with %d errors\n", errorCount);
        else
            printf("TEST PASSED\n");

        // finishing up
        GMIO::free(inArray);
        GMIO::free(outArray);
        delete[] outGolden;
    }

    ret = mygraph.end();

    if (ret != adf::ok)
    {
        printf("End failed\n");
        return -1;
    }
    return 0;
}

// function to benchmark PLIO
int benchmark_plio(graph mygraph)
{
    mygraph.init();

    mygraph.run(1);

    mygraph.end();

    return 0;
}

int main(int argc, char **argv)
{
    std::cout << "----- Memory throughput benchmark -----" << std::endl;
    // With these calls different combinations are benchmarked
    std::cout << "Benchmarking GMIO window: " << std::endl;
    benchmark_gmio(wGraphGMIO.in_window_gmio, wGraphGMIO.out_window_gmio, wGraphGMIO);
    std::cout << "Benchmarking GMIO stream: " << std::endl;
    benchmark_gmio(sGraphGMIO.in_stream_gmio, sGraphGMIO.out_stream_gmio, sGraphGMIO);
    std::cout << "Benchmarking PLIO window: " << std::endl;
    benchmark_plio(wGraphPLIO);
    std::cout << "Benchmarking PLIO stream: " << std::endl;
    benchmark_plio(sGraphPLIO);
    std::cout << "Benchmarking PLIO window 64: " << std::endl;
    benchmark_plio(wGraphPLIO64);
    std::cout << "Benchmarking PLIO stream 64: " << std::endl;
    benchmark_plio(sGraphPLIO64);
    std::cout << "Benchmarking PLIO window 128: " << std::endl;
    benchmark_plio(wGraphPLIO128);
    std::cout << "Benchmarking PLIO stream 128: " << std::endl;
    benchmark_plio(sGraphPLIO128);
    return 0;
}
