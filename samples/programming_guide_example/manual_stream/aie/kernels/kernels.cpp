#include <adf.h>

#define NUM_SAMPLES 32

// NOTE the two kernels do the same thing, we decoupled for the sake of debug

void first(input_stream_cint16 *in, output_stream_cint16 *out)
{
  cint16 c1, c2;

  for (unsigned i = 0; i < NUM_SAMPLES; i++)
  {
    c1 = readincr(in);
#if defined(__AIESIM__) || defined(__X86SIM__)
    printf("[0] Received %d %d\n", c1.real, c2.imag);
#endif
    c2.real = c1.real + c1.imag;
    c2.imag = c1.real - c1.imag;
    writeincr(out, c2);
#if defined(__AIESIM__) || defined(__X86SIM__)
    printf("[0] Done with %d %d\n", c2.real, c2.imag);
#endif
  }
}

void second(input_stream_cint16 *in, output_stream_cint16 *out)
{
  cint16 c1, c2;
  for (unsigned i = 0; i < NUM_SAMPLES; i++)
  {
    c1 = readincr(in);
#if defined(__AIESIM__) || defined(__X86SIM__)
    printf("[1] Received %d %d\n", c1.real, c2.imag);
#endif
    c2.real = c1.real + c1.imag;
    c2.imag = c1.real - c1.imag;
    writeincr(out, c2);
#if defined(__AIESIM__) || defined(__X86SIM__)
    printf("[1] Done with %d %d\n", c2.real, c2.imag);
#endif
  }
}
