#include "graph.h"

/*****************************************************************\
 *  This programm is used for x86simulation and aiesimulation.    *
 *  When PLIO is used this program is just used to start/end      *
 *  the graph.                                                    *
 *  Checking for correct results needs to be done via:            *
 *  diff -w ../data/zint.txt x86simulator_output/data/output.txt  *
 *  or                                                            *
 *  diff -w ../data/zint.txt aiesimulator_output/data/output.txt  *
 \****************************************************************/

simpleGraph mygraph;

#if defined(__AIESIM__) || defined(__X86SIM__)
int main(void)
{
  adf::return_code ret;
  printf("Start program\n");
  mygraph.init();
  printf("Starting graph ....\n");

  ret = mygraph.run(1);
  if (ret != adf::ok)
  {
    printf("Run failed\n");
    return ret;
  }
  ret = mygraph.end();
  if (ret != adf::ok)
  {
    printf("End failed\n");
    return ret;
  }
  return 0;
}
#endif