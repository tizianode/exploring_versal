/*****************************************************************************************\
* The purpose of this benchmark is to measure the aggregated throuhgput between one       *
* sending AI Engine and "nAIE" receiving AI Engines.                                      *
*                                                                                         *
* This program is used for all the possible simulations and the hardware execution.       *
* It is used to:                                                                          *
* - perform the timing of the benchmarks                                                  *
* - initialize and start the AI-Engine graph for different amount of sizes                *
* Various variables are used to define the behaviour of the benchmark:                     *
* - "numOfRuns" defines the number of iterations the benchmark is run                      *
* - "numOfTests" defines the number of tests that are run out of the "numIteration" array  *
* - "numIteration" array defines the different sizes of tests that are run each benchmark  *
*   iteration                                                                             *
\*****************************************************************************************/

#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"

#define __TIMER__
#endif

#ifdef __TIMER__
#include <chrono>
#include <ctime>
class Timer
{
  std::chrono::high_resolution_clock::time_point mTimeStart;

public:
  Timer() { reset(); }
  long long stop()
  {
    std::chrono::high_resolution_clock::time_point timeEnd =
        std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                 mTimeStart)
        .count();
  }
  void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};

#endif

// These are the various numbers of iteration that the kernel is executed
// the resulting sent data sizes are given as comments
const int numIterations[23] = {
    1,       //   8 KB
    2,       //  16 KB
    4,       //  32 KB
    8,       //  64 KB
    16,      // 128 KB
    32,      // 256 KB
    64,      // 512 KB
    128,     //   1 MB
    256,     //   2 MB
    512,     //   4 MB
    1024,    //   8 MB
    2048,    //  16 MB
    4096,    //  32 MB
    8192,    //  64 MB
    16384,   // 128 MB
    32768,   // 256 MB
    65536,   // 512 MB
    131072,  //   1 GB
    262144,  //   2 GB
    524288,  //   4 GB
    1048576, //   8 GB
    2097152, //  16 GB
    4194304  //  32 GB
};

simpleGraph mygraph;
int main(int argc, char *argv[])
{
  // Setup standard values
  int numOfRuns = 1;
  int numOfTests = 23;
  unsigned long long actual_size;
  adf::return_code ret;

  // Setup benchmark data file
  std::string csvName = "P2P.csv";
  std::ofstream ResultFile(csvName);
  ResultFile << "nAIE,NumOfSamples,Throughput\n";

  // Get user input only if not in x86simulation or aiesimulation
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc < 3)
  {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << "<xclbin file> <no of iterations>" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
  unsigned long long input = atoi(argv[2]);

  numOfRuns = input;

  if (input == 1)
  {
    numOfTests = 1;
  }

#else // if in simulation use small numbers of iterations
  numOfRuns = 1;
  numOfTests = 1; //<--------------- set the number of iterations
#endif

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  // Create XRT device handle for ADF API
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif

  unsigned long long iterations;
  double throughput = 0;

  std::cout << "Number of Tests: " << numOfTests << std::endl;
  std::cout << "Number of Runs: " << numOfRuns << std::endl;

  for (int j = 0; j < numOfRuns; j++)
    for (int i = 0; i < numOfTests; i++)
    {

      iterations = numIterations[i];
      actual_size = iterations * WINDOW_SIZE;

      // compute the number of elements that must be transferred
      unsigned long long length = ceil(actual_size / sizeof(int32));
      std::cout << "Iteration " << j << ": Sending " << length << " elements (" << actual_size << " bytes). # of kernel iterations: " << iterations << "." << std::endl;

      mygraph.init();

#ifdef __TIMER__
      // timing
      Timer timer;
#endif
      // std::cout << "Host: Starting ... " << std::endl;
      ret = mygraph.run(iterations);

      if (ret != adf::ok)
      {
        printf("Host: Run failed\n");
        return ret;
      }

      mygraph.wait(); // wait for iteration(s) to finish
#ifdef __TIMER__
      long long timer_stop = timer.stop();
      // printf("Execution time (usecs): %lld\n", timer_stop);
      throughput = ((actual_size * nAIE / timer_stop) * 1e6) / 1024 / 1024;
      // printf("Throughput (MB/s): %f\n", throughput);
#endif
      ResultFile << nAIE << "," << (actual_size / 1024 > 1024 ? (actual_size / 1024 / 1024 > 1024 ? std::to_string(actual_size / 1024 / 1024 / 1024) + " GB" : std::to_string(actual_size / 1024 / 1024) + " MB") : std::to_string(actual_size / 1024) + " KB") << "," << throughput << std::endl;
    }

  ResultFile.close();

  ret = mygraph.end();
  if (ret != adf::ok)
  {
    printf("End failed\n");
    return ret;
  }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}
