# Versal ACAP Benchmarks

A set of mini applications and benchmarks targeting Xilinx Versal ACAP devices.

All code has been tested for the VCK 190 Evaluation board.

# Structure
- ```samples/programming_guide_example/```: this includes tutorial 05 from Xilinx
- [```samples/vec_sum```](samples/vec_sum/vec_sum.md): Various versions of vector summation
- [```benchmarks/AIE/communications/point_to_point```](benchmarks/AIE/communications/point_to_point/README.md): Benchmark for the on chip inter AIE point-to-point communication
- [```benchmarks/AIE/communications/point_to_multiple```](benchmarks/AIE/communications/point_to_multiple/README.md): Benchmark for the out-of-the-box boradcast function
  
- [```benchmarks/memory_througput```](benchmarks/memory_throughput/README.md): Various versions of different memory throughput benchmarks

There is a Makefile for each process step.
This is explained in the subfolders.
