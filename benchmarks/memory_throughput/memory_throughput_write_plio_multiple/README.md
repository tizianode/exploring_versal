# DDR memory write throughput benchmarking with multiple AI-Engines using PLIO

This program measures the accumulated write throughput between the DDR memory and the AI-Engines. For that purpose it uses `nAIE` (defined in `aie/graph.h`) AI-Engines to write data simultaneously from the DDR memory. This program was only implemented for PLIO and windows. 

For instructions on how to run the benchmark see `../README.md` 

The resulting `host.exe` will then run for the specified amount of iterations (`host.cpp`: numOfRuns) and sizes of chunks of data (`host.cpp`: numSamples[] and numOfTests). It automatically generates some .csv files with tuples of `nAIE`, chunksize and measured throughput.
