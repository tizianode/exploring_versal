#include <adf.h>
#include "./kernels/window.h"


using namespace adf;

// define the number of AIEs to be used
#define nAIE 4

class windowGraphGMIO : public graph
{
private:
    kernel kern_window_gmio[nAIE];

public:
    input_gmio in_window_gmio[nAIE];
    output_gmio out_window_gmio[nAIE];

    windowGraphGMIO()
    {

        printf("Init\n");

        // create gmio
        for (int k = 0; k < nAIE; k++)
        {
            printf("Creating GMIO %d\n", k);
            in_window_gmio[k] = input_gmio::create("in_window_gmio" + std::to_string(k), 64, 1000);
            out_window_gmio[k] = output_gmio::create("out_window_gmio" + std::to_string(k), 64, 1000);

            kern_window_gmio[k] = kernel::create(kernel_window);

            connect<window<NUM_SAMPLES * sizeof(int)>>(in_window_gmio[k].out[0], kern_window_gmio[k].in[0]);
            connect<window<2 * sizeof(int)>>(kern_window_gmio[k].out[0], out_window_gmio[k].in[0]);

            source(kern_window_gmio[k]) = "kernels/window.cpp";

            runtime<ratio>(kern_window_gmio[k]) = 1;
        }
    }
};
