#!/bin/bash

# generate 32-bit PLIO formatted input
echo -n "" > in.txt
# generate 32-bit PLIO formatted output
echo -n "" > golden.txt
for ((c=0; c<2048; c++))
do
    echo "1 " >> golden.txt
done

# generate 64-bit PLIO formated output
echo -n "" > golden_64.txt
for ((c=0; c<1024; c++))
do
    echo "1 1 " >> golden_64.txt
done

# generate 128-bit PLIO formatted input
echo -n "" > golden_128.txt
for ((c=0; c<512; c++))
do
    echo "1 1 1 1 " >> golden_128.txt
done