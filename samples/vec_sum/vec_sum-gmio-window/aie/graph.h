#include <adf.h>
#include "kernels/vectorised.h"

using namespace adf;

// NOTES:
//  - printf seems to be useful for x86sim, but the stdout is flushed only at the end of x86simulator

class simpleGraph : public graph
{
private:
  kernel kern;

public:
  input_gmio in_x, in_y;
  output_gmio out;

  simpleGraph()
  {

    printf("Init\n");

    // create gmio
    in_x = input_gmio::create("in_x", 64, 1000);
    in_y = input_gmio::create("in_y", 64, 1000);
    out = output_gmio::create("out", 64, 1000);
    printf("GMIO created\n");

    kern = kernel::create(vecsum_kernel);

    // use window to communicate: note that the window must have enough data
    // to have it sent
    connect<window<NUM_SAMPLES * sizeof(float)>> net0(in_x.out[0], kern.in[0]);
    connect<window<NUM_SAMPLES * sizeof(float)>> net1(in_y.out[0], kern.in[1]);
    connect<window<NUM_SAMPLES * sizeof(float)>> net2(kern.out[0], out.in[0]);

    // give source to the kernel, depending which one you want
    source(kern) = "kernels/vectorised.cpp";

    runtime<ratio>(kern) = 0.1;
  }
};
