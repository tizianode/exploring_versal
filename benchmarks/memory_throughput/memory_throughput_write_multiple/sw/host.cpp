/*******************************************************************************\
* This host program can be used to benchmark the throughput                     *
* between multiple ("nAIE") AIEs and the DDR Memory of the board                *
* (i.e. memory read). "nAIE" is defined and can be changed in graph.h.           *
* This was here only implemented for GMIO based data transfer in combination    *
* with windows and streams.                                                     *
* The variables "numOfRuns", "numOfTests" and "numSamples" are used to define:   *
* "numOfRuns": How often should the benchmark be run                            *
* "numOfTests": until what index of the numSamples array should be tested       *
* "numSamples": an array containing different number of samples that will be    *
*               sent/read                                                       *
* Note that the maximum allocatable memory is bound by the CMA of the OS, in    *
* this case 1 GB (see line 95)                                                  *
* The throughput results of the benchmark are then written to a csv file which   *
* name is defined in line 76.                                                    *
\*******************************************************************************/

#include "../aie/graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>
#include <iostream>
#include <string>

// To use GMIO in Hardware flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
// This is used for the PL Kernels
#include "xrt.h"

#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"

// Used for basic timing
#include <chrono>
#include <ctime>
class Timer
{
    std::chrono::high_resolution_clock::time_point mTimeStart;

public:
    Timer() { reset(); }
    long long stop()
    {
        std::chrono::high_resolution_clock::time_point timeEnd =
            std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                     mTimeStart)
            .count();
    }
    void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};
#endif

using namespace adf;

// Set the number of Iterations and number of Tests to be done
const int numOfRuns = 1;
const int numOfTests = 5;
const int numSamples[5] = {
    4194304,  // 16 MB
    8388608,  // 32 MB
    16777216, // 64 MB
    33554432, // 128 MB
    67108864, // 256 MB
};

// function specifically for the benchmarking of GMIO based connections
int benchmark_gmio(streamGraphGMIO mygraph)
// int benchmark_gmio(windowGraphGMIO mygraph)
{
    auto throughputs = new float[numOfTests];
    adf::return_code ret;

    // Automatic creation of a csv file for analysis
    // std::string csvName = "GMIO-window-multiple-out-nAIE" + std::to_string(nAIE) + ".csv";
    std::string csvName = "GMIO-stream-multiple-out-nAIE" + std::to_string(nAIE) + ".csv";
    std::ofstream ResultFile(csvName);
    ResultFile << "nAIE,DataSent,Throughput\n";

    // Starting graph
    mygraph.init();
    // Number of iterations to be executed
    for (int j = 0; j < numOfRuns; j++)
    {
        // until what size of samples
        for (int i = 0; i < numOfTests; i++)
        {
            int n = numSamples[i];
            if (n % NUM_SAMPLES != 0)
            {
                std::cout << n << " is not multiple of" << NUM_SAMPLES << "! " << n % NUM_SAMPLES << std::endl;
                return -1;
            }
            // Stop if allocated memory would be bigger than CMA
            if (n * nAIE * 4 >= 1073741824)
                break;

            std::cout << "Iteration " << j << ": Running Tests for " << n << " Samples and " << nAIE << " AIEs." << std::endl;

            int NUM_SAMPLES_TOTAL = n;                                  // Number of Data samples that is sent in total
            int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;           // Number of Iterations the kernels have to run
            int BLOCK_SIZE_out_Bytes = NUM_SAMPLES_TOTAL * sizeof(int); // Amount of Bytes we need to reserve in the off-chip memory to store the data

            // GMIO Malloc all needed arrays
            int *outArray[nAIE];
            std::cout << "Allocating outarrays" << std::endl;

            for (int k = 0; k < nAIE; k++)
            {
                outArray[k] = (int *)GMIO::malloc(BLOCK_SIZE_out_Bytes);
            }

            // Fill reference array
            int *outGolden = new int[NUM_SAMPLES_TOTAL];
            for (int k = 0; k < NUM_SAMPLES_TOTAL; k++)
                outGolden[k] = 1;

            // run the graph
            std::cout << "Graph started" << std::endl;
            ret = mygraph.run(ITERATIONS);

            if (ret != adf::ok)
            {
                printf("Run failed\n");
                return -1;
            }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
            const int WINDOW_SIZE_in_bytes = NUM_SAMPLES * sizeof(int); // Number of samples per iteration * sizeof(int)
            // start the timer
            Timer timer;
#endif
            // get the ouput from the aies
            for (int k = 0; k < nAIE; k++)
            {
                mygraph.out_stream_gmio[k].aie2gm_nb(outArray[k], BLOCK_SIZE_out_Bytes);
            }

            // wait for the memory transactions to finish
            for (int k = 0; k < nAIE; k++)
            {
                mygraph.out_stream_gmio[k].wait();
            }

            // Timed part end
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
            // stop the timer
            double timer_stop = timer.stop();
            double throughputTimer = ((double)WINDOW_SIZE_in_bytes * ITERATIONS * nAIE) / 1024 / 1024 / timer_stop * 1000000; // Mbytes per second
            throughputs[i] = throughputTimer;
#endif

            int errorCount = 0;
            // Verification of the results
            for (int k = 0; k < nAIE; k++)
                for (int j = 0; j < NUM_SAMPLES_TOTAL; j++)
                {
                    if (outArray[k][j] != outGolden[j])
                    {
                        std::cout << "ERROR:dout[" << j << "]=" << outArray[k][j] << ",gold=" << outGolden[j] << std::endl;
                        errorCount++;
                    }
                }
            // Wait for graph to finish, before starting it again
            mygraph.wait();

            if (errorCount)
                printf("Test failed with %d errors\n", errorCount);
            else
                printf("TEST PASSED\n");

            // finishing up
            for (int k = 0; k < nAIE; k++)
                GMIO::free(outArray[k]);
            delete[] outGolden;
        }

        // Saving the throughput results
        for (int i = 0; i < numOfTests; i++)
        {
            ResultFile << nAIE << "," << (numSamples[i] * 4 / 1024 > 1024 ? std::to_string(numSamples[i] * 4 / 1024 / 1024) + " MB" : std::to_string(numSamples[i] * 4 / 1024) + " KB") << "," << throughputs[i] << "\n";
        }
    }

    ResultFile.close();

    // close graph
    ret = mygraph.end();
    if (ret != adf::ok)
    {
        printf("End failed\n");
        return -1;
    }

    return 1;
}

static std::vector<char>
load_xclbin(xrtDeviceHandle device, const std::string &fnm)
{
    if (fnm.empty())
        throw std::runtime_error("No xclbin specified");

    // load bit stream
    std::ifstream stream(fnm);
    stream.seekg(0, stream.end);
    size_t size = stream.tellg();
    stream.seekg(0, stream.beg);

    std::vector<char> header(size);
    stream.read(header.data(), size);

    auto top = reinterpret_cast<const axlf *>(header.data());
    if (xrtDeviceLoadXclbin(device, top))
        throw std::runtime_error("Xclbin loading failed");

    return header;
}

int main(int argc, char **argv)
{
    std::cout << "----- Benchmarks -----" << std::endl;

// To use GMIO in the Hardware  flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
    // Create XRT device handle for ADF API
    char *xclbinFilename = argv[1];
    auto dhdl = xrtDeviceOpen(0); // device index=0
    auto xclbin = load_xclbin(dhdl, xclbinFilename);
    auto top = reinterpret_cast<const axlf *>(xclbin.data());

    adf::registerXRT(dhdl, top->m_header.uuid);
#endif
    // to benchmark GMIO window you first need to uncomment the wGraphGMIO in the graph.h
    // and comment the sGraphGMIO. Depending on nAIE there are not enough GMIO ports available
    std::cout << "----- Memorythroughput benchmark -----" << std::endl;
    std::cout << "Benchmarking GMIO stream: " << std::endl;
    // std::cout << "Benchmarking GMIO window: " << std::endl;
    streamGraphGMIO sGraphGMIO;
    // windowGraphGMIO wGraphGMIO;
    benchmark_gmio(sGraphGMIO);
    // benchmark_gmio(wGraphGMIO);

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
    xrtDeviceClose(dhdl);
#endif

    return 0;
}
