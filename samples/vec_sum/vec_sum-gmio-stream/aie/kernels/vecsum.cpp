#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "vecsum.h"

/**************************************************************************\
* Kernel to sum two vectors x and y                                        *
* This kernel receives (and sends) its input and output data over streams. *
* Due to the low throughput of streams there is no need to vectorise       *
* the computation.                                                         *
\**************************************************************************/

void vecsum_kernel(input_stream_float *in_x, input_stream_float *in_y, output_stream_float *out)
{
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Kernel started!!\n");
#endif
  float x, y, z;
  for (int i = 0; i < NUM_SAMPLES; i++)
  {
    x = readincr(in_x);
    y = readincr(in_y);
    z = x + y;
    writeincr(out, z);
  }
}
