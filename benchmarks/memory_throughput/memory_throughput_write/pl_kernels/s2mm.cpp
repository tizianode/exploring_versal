
#include <ap_int.h>
#include <hls_stream.h>
#include <ap_axi_sdata.h>

extern "C"
{
    // Receives data from stream and stores it in memory
    // This is done with a width of 32, 64 or 128 bit

    void __PLIO_WIDTH_NAME__(ap_int<__PLIO_WIDTH__> *mem, hls::stream<qdma_axis<__PLIO_WIDTH__, 0, 0, 0>> &s, int size)
    {
#pragma HLS INTERFACE m_axi port = mem offset = slave bundle = gmem

#pragma HLS interface axis port = s

#pragma HLS INTERFACE s_axilite port = mem bundle = control
#pragma HLS INTERFACE s_axilite port = size bundle = control
#pragma HLS interface s_axilite port = return bundle = control

        for (int i = 0; i < size; i++)
        {
#pragma HLS PIPELINE II = 1
            qdma_axis<__PLIO_WIDTH__, 0, 0, 0> x = s.read();
            mem[i] = x.data;
        }
    }
}
