# DDR memory simultaneously read and write throughput benchmarking with multiple AI-Engines

This programm measures the accumulated read and write throughput between the DDR memory and the AI-Engines. For that purpose it uses `nAIE` (defined in `aie/graph.h`) identical kernels on `nAIE` distinct AI-Engines to read data and `nAIE` identical kernels on `nAIE` distinct AI-Engines to write data simultaneously to the DDR memory. This program was only implemented for GMIO with windows and streams. 

Right now it is configured to use the graph with GMIO and streams, if you want to use GMIO and windows you have to uncomment the `windowGraphGMIO` in `aie/graph.h` and comment the `streamGraphGMIO`. And also comment and uncomment the corresponding parts in the `sw/host.cpp` file.

For instructions on how to run the benchmark see `../README.md` 

The resulting `host.exe` will then run for the specified amount of iterations (`host.cpp`: numOfRuns) and sizes of chunks of data (`host.cpp`: numSamples[] and numOfTests). It automatically generates some .csv files with tuples of `nAIE`, chunksize and measured throughput.
