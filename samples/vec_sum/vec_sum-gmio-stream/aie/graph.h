#include <adf.h>
#include "kernels/vecsum.h"

using namespace adf;

// NOTES:
//  - printf seems to be useful for x86sim, but the stdout is flushed only at the end of x86simulator

class simpleGraph : public graph
{
private:
  kernel kern;

public:
  input_gmio in_x, in_y;
  output_gmio out;

  simpleGraph()
  {

    printf("Init\n");

    // create gmio
    in_x = input_gmio::create("in_x", 256, 4000);
    in_y = input_gmio::create("in_y", 256, 4000);
    out = output_gmio::create("out", 256, 4000);
    printf("GMIO created\n");

    kern = kernel::create(vecsum_kernel);

    // use stream to communicate
    connect<stream> net0(in_x.out[0], kern.in[0]);
    connect<stream> net1(in_y.out[0], kern.in[1]);
    connect<stream> net2(kern.out[0], out.in[0]);

    // give source to the kernel
    source(kern) = "kernels/vecsum.cpp";

    runtime<ratio>(kern) = 0.1;
  }
};
