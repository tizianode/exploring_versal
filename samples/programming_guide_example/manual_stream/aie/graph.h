#include <adf.h>
#include "kernels/kernels.h"

using namespace adf;

class simpleGraph : public graph
{
private:
  kernel firstk;
  kernel secondk;

public:
  input_plio in;
  output_plio out;
  simpleGraph()
  {

    in = input_plio::create("DataIn", plio_32_bits, "../../data/input.txt");
    out = output_plio::create("DataOut", plio_32_bits, "../../data/output.txt");

    firstk = kernel::create(first);
    secondk = kernel::create(second);
    connect<stream> net0(in.out[0], firstk.in[0]);
    connect<stream> net1(firstk.out[0], secondk.in[0]);
    connect<stream> net2(secondk.out[0], out.in[0]);

    source(firstk) = "kernels/kernels.cpp";
    source(secondk) = "kernels/kernels.cpp";

    runtime<ratio>(firstk) = 0.1;
    runtime<ratio>(secondk) = 0.1;
  }
};
