/* A simple kernel
 */
#include <adf.h>
#include "kernels.h"

/**
  Sender kernel: sends "1"s to all of the receivers
  The amount of sent "1"s is defined via the "size" function argument
*/

#define VECTOR_LENGTH 8

void sender(output_window<int32> *out, int64 size)
{
  const uint32_t length = size;
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Sender, sending %u elements\n", length);
#endif

  v8int32 basisOne = null_v8int32();

  for (int i = 0; i < VECTOR_LENGTH; i++)
  {
    basisOne = upd_elem(basisOne, i, 1);
  }

  for (unsigned i = 0; i < length / VECTOR_LENGTH; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_writeincr(out, basisOne);
    }
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Sender, completed\n");
#endif
}

/**
  Receiving kernel: receives all the data sent from the sending kernel
  The exact amount is specified via the "size" function argument. 
*/
void receiver(input_window<int32> *in, int64 size)
{
  const uint32_t length = size;

  v8int32 xs;

#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Receiver, receiving %u elements\n", length);
#endif
  for (unsigned i = 0; i < length / VECTOR_LENGTH; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_readincr(in, xs);
    }
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("last element received= %d \n", xs);
#endif
}
