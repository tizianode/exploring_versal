# DDR memory write throughput benchmarking from DDR to AI-Engines

This is a small program that uses the different methods of data access between AI-Engines and the DDR Memory on the chip.  
The options tested include the following:
- GMIO and window/streams
- PLIO and window/streams (with PLIO widths of 32bit)

For instructions on how to run the benchmark see `../README.md` 

The resulting `host.exe` will then run for the specified amount of iterations (`host.cpp`: numOfRuns) and sizes of chunks of data (`host.cpp`: numSamples[] and numOfTests). It automatically generates some .csv files with pairs of chunksize and measured throughput.
