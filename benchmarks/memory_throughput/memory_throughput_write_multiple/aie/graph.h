#ifndef __GRAPH_H_
#define __GRAPH_H_
#include <adf.h>
#include "kernels/window.h"
#include "kernels/stream.h"

using namespace adf;

// Defines the number of used AIES and therefore GMIO ports
#define nAIE 10

// Uncomment the graph you want to use in graph.cpp lines 16 and 17 and host.cpp lines 234 and 235
class windowGraphGMIO : public graph
{
private:
    kernel kern_window_gmio[nAIE];

public:
    output_gmio gmioOut[nAIE];
    windowGraphGMIO()
    {
        printf("Init\n");
        // create gmio
        for (int i = 0; i < nAIE; i++)
        {
            printf("Creating GMIO %d:\n", i);
            gmioOut[i] = output_gmio::create("gmioOut" + std::to_string(i), 64, 500);
            kern_window_gmio[i] = kernel::create(kernel_window);
            connect<window<NUM_SAMPLES * sizeof(int)>>(kern_window_gmio[i].out[0], gmioOut[i].in[0]);
            source(kern_window_gmio[i]) = "kernels/window.cpp";
            runtime<ratio>(kern_window_gmio[i]) = 1;
        }
    };
};

class streamGraphGMIO : public graph
{
private:
    kernel kern_stream_gmio[nAIE];

public:
    output_gmio out_stream_gmio[nAIE];

    streamGraphGMIO()
    {

        printf("Init\n");

        // create gmios
        for (int i = 0; i < nAIE; i++)
        {
            printf("Creating GMIO %d:\n", i);
            out_stream_gmio[i] = output_gmio::create("out_stream_gmio" + std::to_string(i), 64, 500);
            kern_stream_gmio[i] = kernel::create(kernel_stream);
            connect<stream>(kern_stream_gmio[i].out[0], out_stream_gmio[i].in[0]);
            source(kern_stream_gmio[i]) = "kernels/stream.cpp";
            runtime<ratio>(kern_stream_gmio[i]) = 1;
        }
    }
};
#endif