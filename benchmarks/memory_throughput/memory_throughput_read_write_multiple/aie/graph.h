#ifndef __GRAPH_H_
#define __GRAPH_H_
#include <adf.h>
#include "kernels/window_out.h"
#include "kernels/window_in.h"
#include "kernels/stream_out.h"
#include "kernels/stream_in.h"

using namespace adf;

// Defines the number of used AIES and therefore GMIO ports
#define nAIE 3

// this graph uses nAIE number of AI-Engines to generate memory transfers.
// for that purpose it generates nAIE AIEs that read data and nAIE AIEs that write data to the DDR memory
// with this graph it uses windows and GMIO for that, the other one uses streams

// NOTE: only one of those two graphs can be active at the same time. To change the graph comment and uncomment
// the coresponding graph in graph.cpp lines 16 or 17 and in host.cpp lines 282ff

class windowGraphGMIO : public graph
{
private:
    kernel kern_window_gmio_out[nAIE];
    kernel kern_window_gmio_in[nAIE];

public:
    output_gmio gmioOut[nAIE];
    input_gmio gmioIn[nAIE];
    output_gmio gmioOutIn[nAIE];

    windowGraphGMIO()
    {
        printf("Init\n");
        // create output kernels
        for (int i = 0; i < nAIE; i++)
        {
            printf("Creating GMIO out %d:\n", i);
            gmioOut[i] = output_gmio::create("gmioOut" + std::to_string(i), 64, 100);
            kern_window_gmio_out[i] = kernel::create(kernel_window_out);
            connect<window<NUM_SAMPLES * sizeof(int)>>(kern_window_gmio_out[i].out[0], gmioOut[i].in[0]);
            source(kern_window_gmio_out[i]) = "kernels/window_out.cpp";
            runtime<ratio>(kern_window_gmio_out[i]) = 1;
        }
        // create input kernels
        for (int i = 0; i < nAIE; i++)
        {
            printf("Creating GMIO in %d:\n", i);
            gmioIn[i] = input_gmio::create("gmioIn" + std::to_string(i), 64, 100);
            gmioOutIn[i] = output_gmio::create("gmioOutIn" + std::to_string(i), 64, 100);
            kern_window_gmio_in[i] = kernel::create(kernel_window_in);
            connect<window<NUM_SAMPLES * sizeof(int)>>(gmioIn[i].out[0], kern_window_gmio_in[i].in[0]);
            connect<window<2 * sizeof(int)>>(kern_window_gmio_in[i].out[0], gmioOutIn[i].in[0]);
            source(kern_window_gmio_in[i]) = "kernels/window_in.cpp";
            runtime<ratio>(kern_window_gmio_in[i]) = 1;
        }
    };
};

class streamGraphGMIO : public graph
{
private:
    kernel kern_stream_gmio_out[nAIE];
    kernel kern_stream_gmio_in[nAIE];

public:
    output_gmio gmioOut[nAIE];
    input_gmio gmioIn[nAIE];
    output_gmio gmioOutIn[nAIE];

    streamGraphGMIO()
    {
        printf("Init\n");
        // create output kernels
        for (int i = 0; i < nAIE; i++)
        {
            printf("Creating GMIO out %d:\n", i);
            gmioOut[i] = output_gmio::create("gmioOut" + std::to_string(i), 64, 100);
            kern_stream_gmio_out[i] = kernel::create(kernel_stream_out);
            connect<stream>(kern_stream_gmio_out[i].out[0], gmioOut[i].in[0]);
            source(kern_stream_gmio_out[i]) = "kernels/stream_out.cpp";
            runtime<ratio>(kern_stream_gmio_out[i]) = 1;
        }
        // create input kernels
        for (int i = 0; i < nAIE; i++)
        {
            printf("Creating GMIO in %d:\n", i);
            gmioIn[i] = input_gmio::create("gmioIn" + std::to_string(i), 64, 100);
            gmioOutIn[i] = output_gmio::create("gmioOutIn" + std::to_string(i), 64, 100);
            kern_stream_gmio_in[i] = kernel::create(kernel_stream_in);
            connect<stream>(gmioIn[i].out[0], kern_stream_gmio_in[i].in[0]);
            connect<stream>(kern_stream_gmio_in[i].out[0], gmioOutIn[i].in[0]);
            source(kern_stream_gmio_in[i]) = "kernels/stream_in.cpp";
            runtime<ratio>(kern_stream_gmio_in[i]) = 1;
        }
    };
};
#endif
