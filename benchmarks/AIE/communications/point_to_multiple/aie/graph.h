
#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nAIE 200

// This is a basic graph that contains one sending kernel and nAIE receiving kernels
class simpleGraph : public graph
{
private:
  kernel senderk;
  kernel receiverk[nAIE];

public:
  input_port size_value_snd, size_value_rcv;
  simpleGraph()
  {

    // Set up the sender
    senderk = kernel::create(sender);
    source(senderk) = "kernels.cpp";
    runtime<ratio>(senderk) = 1;
    location<kernel>(senderk) = tile(0, 0);

    // Setup the nAIE receivers
    for (int i = 0; i < nAIE; i++)
    {
      receiverk[i] = kernel::create(receiver);

      // data window from sender to receiver
      connect<window<WINDOW_SIZE>> net0(senderk.out[0], receiverk[i].in[0]);
      source(receiverk[i]) = "kernels.cpp";
      runtime<ratio>(receiverk[i]) = 1;
      // Introduce location constraints so that no reciever is direct neighbour of the sender
      if (i < 50)
      {
        location<kernel>(receiverk[i]) = tile(i, 7);
      }
      else if (i < 100)
      {
        location<kernel>(receiverk[i]) = tile(i % 50, 6);
      }
      else if (i < 150)
      {
        location<kernel>(receiverk[i]) = tile(i % 50, 5);
      }
      else if (i < 200)
      {
        location<kernel>(receiverk[i]) = tile(i % 50, 4);
      }
    }
  }
};
