../../ #include<adf.h>
#include "kernels/vecsum.h"

    using namespace adf;

// NOTES:
//  - printf seems to be useful for x86sim, but the stdout is flushed only at the end of x86simulator

class simpleGraph : public graph
{
private:
  kernel kern;

public:
  // input_plio in_x;
  input_plio in_x, in_y;
  output_plio out;

  simpleGraph()
  {

    printf("Init\n");

    // custom dataset
    in_x = input_plio::create("in_x", plio_32_bits, "../../data/xint.txt");
    in_y = input_plio::create("in_y", plio_32_bits, "../../data/yint.txt");
    out = output_plio::create("out", plio_32_bits, "../../data/output.txt");
    printf("PLIO CREATED\n");

    kern = kernel::create(vecsum_kernel);

    // use stream to communicate
    connect<stream> net0(in_x.out[0], kern.in[0]);
    connect<stream> net1(in_y.out[0], kern.in[1]);
    connect<stream> net2(kern.out[0], out.in[0]);

    // give source to the kernel, depending which one you want
    source(kern) = "kernels/vecsum.cpp";

    runtime<ratio>(kern) = 0.1;
  }
};
