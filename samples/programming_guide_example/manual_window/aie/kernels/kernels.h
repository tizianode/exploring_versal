#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
void first(input_window<cint16> * in, output_window<cint16> * out);
void second(input_window<cint16> * in, output_window<cint16> * out);

#endif
