#!/bin/bash

# generate 32-bit PLIO formatted input
echo -n "" > in.txt
for ((c=0; c<2047; c++))
do
    echo "1" >> in.txt
done
 echo -n "1" >> in.txt
# generate 32-bit PLIO formatted output
echo -n "" > golden.txt
echo "2048 " >> golden.txt
echo "0 " >> golden.txt


# generate 64-bit PLIO formated input
echo -n "" > in64.txt
for ((c=0; c<1023; c++))
do
    echo "1 1" >> in64.txt
done
echo -n "1 1" >> in64.txt
# generate 64-bit PLIO formatted output
echo -n "" > golden64.txt
echo "2048 0 " >> golden64.txt

# generate 128-bit PLIO formatted input
echo -n "" > in128.txt
for ((c=0; c<511; c++))
do
    echo "1 1 1 1" >> in128.txt
done
echo -n "1 1 1 1" >> in128.txt
# generate 128-bit PLIO formatted output
echo -n "" > golden128.txt
echo "2048 0 0 0 " >> golden128.txt