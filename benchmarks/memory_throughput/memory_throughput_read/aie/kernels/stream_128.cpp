#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "stream_128.h"

/*
  Kernel to measure off chip memory read bandwidth using streams
  This kernel is set up to work with 128 bit width PLIO connections
  (this is the reason why we writeincr out four times, then we have 128 bit)

  As you can see from the code, all stream based kernels are run for a constant amount of samples.
  While we are specifing this number of samples at compile time (see NUM_SAMPLES in stream_128.h),
  this could also be implemented using a runtime parameter. Then it would be possible to change the amount during runtime.
*/
#define VECTOR_LEN 4

void kernel_stream_128(input_stream<int> *in, output_stream<int> *out)
{
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Started kernel stream 128\n");
#endif

  v4int32 xV = null_v4int32();
  v4int32 zV = null_v4int32();

  for (int i = 0; i < NUM_SAMPLES / 4; i++)
  {
    xV = readincr_v4(in);

    zV = operator+(xV, zV);
  }

  int z = 0;
  for (unsigned i = 0; i < VECTOR_LEN; i++)
  {
    z += ext_elem(zV, i);
  }
  zV = upd_elem(zV, 0, z);
  zV = upd_elem(zV, 1, 0);
  zV = upd_elem(zV, 2, 0);
  zV = upd_elem(zV, 3, 0);

#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Writing output... z: %d\n", z);
#endif
  writeincr(out, z);
  writeincr(out, 0);
  writeincr(out, 0);
  writeincr(out, 0);
}
