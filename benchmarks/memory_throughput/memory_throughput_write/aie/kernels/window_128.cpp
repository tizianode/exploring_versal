#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "window_128.h"

/*
  Kernel to measure off chip memory write bandwidth using windows
*/
#define VECTOR_LEN 8

void kernel_window_128(output_window<int> *out)
{
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Started kernel window 128\n");
#endif
  v8int32 xV = null_v8int32();
  for (int i = 0; i < VECTOR_LEN; i++)
  {
    xV = upd_elem(xV, i, 1);
  }

  const unsigned LSIZE = (NUM_SAMPLES / VECTOR_LEN);
  for (unsigned i = 0; i < LSIZE; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_writeincr(out, xV);
    }
}