#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "stream_out.h"

/*
  Kernel to measure off chip memory write bandwidth using streams

  As you can see from the code, all stream based kernels are run for a constant amount of samples.
  While we are specifing this number of samples at compile time (see NUM_SAMPLES in stream_out.h),
  this could also be implemented using a runtime parameter. Then it would be possible to change the amount during runtime.
*/

void kernel_stream_out(output_stream<int> *out)
{
  for (int i = 0; i < NUM_SAMPLES; i++)
  {
    writeincr(out, 1);
  }
}
