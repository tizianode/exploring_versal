#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "window_128.h"

/*
  Kernel to measure off chip memory read bandwidth using windows
  This kernel is set up to work with 128 bit width PLIO connections
  (this is the reason why we writeincr out four times at the end, then we send 128 bits.
  Otherwise the PLIO transaction wouldn't start since it doesn't have 128 bits to send. The kernel would stall.)
*/
#define VECTOR_LEN 8

void kernel_window_128(input_window<int> *in, output_window<int> *out)
{
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Started kernel window 128\n");
#endif

  v8int32 xV = null_v8int32();
  v8int32 zV = null_v8int32();

  const unsigned LSIZE = (NUM_SAMPLES / VECTOR_LEN);
  for (unsigned i = 0; i < LSIZE; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_readincr(in, xV);
      zV = operator+(xV, zV);
    }

  int z = 0;
  for (unsigned i = 0; i < VECTOR_LEN; i++)
  {
    z += ext_elem(zV, i);
  }
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Z: %d\n", z);
#endif

  window_writeincr(out, z);
  window_writeincr(out, 0);
  window_writeincr(out, 0);
  window_writeincr(out, 0);
}