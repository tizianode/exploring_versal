# DDR memory read throughput benchmarking with multiple AI-Engines

This program measures the accumulated read throughput between the DDR memory and the AI-Engines. For that purpose it uses `nAIE` (defined in `aie/graph.h`) identical kernels on `nAIE` distinct AI-Engines to read data simultaneously from the DDR memory. This program was only implemented for GMIO and windows. 

For instructions on how to run the benchmark see `../README.md` 

The resulting `host.exe` will then run for the specified amount of iterations (`host.cpp`: numOfRuns) and sizes of chunks of data (`host.cpp`: numSamples[] and numOfTests). It automatically generates some .csv files with pairs of chunksize and measured throughput.
