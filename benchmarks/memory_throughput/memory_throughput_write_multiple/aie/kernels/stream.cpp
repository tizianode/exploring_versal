#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "stream.h"

/*
  Kernel to measure off chip memory write bandwidth using streams
  This kernel is set up to work with GMIO or 32 bit width PLIO connections

  As you can see from the code, all stream based kernels are run for a constant amount of samples.
  While we are specifing this number of samples at compile time (see NUM_SAMPLES in stream.h),
  this could also be implemented using a runtime parameter. Then it would be possible to change the amount during runtime.
*/

void kernel_stream(output_stream<int> *out)
{
  for (int i = 0; i < NUM_SAMPLES; i++)
  {
    writeincr(out, 1);
  }
}
