#include "graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>

/***********************************************************************************\
 *  This programm is used for sw_emulation, hw_emulation and hw_execution.          *
 *  It:                                                                             *
 *  - allocates memory that is needed for the input/output of the kernels           *
 *  - arranges the data transfer with the AI Engines (in this case GMIO is used)    *
 *  - checks that the returned data checks out with the expected results            *
 *  In this case it is also used to do some timings on the program                  *
 \**********************************************************************************/

// To use GMIO in Hardware flow
// In theory these checks should be able to identify whether we are running in
// hardware or not, but they can't seperate between hw_emulation and sw_emulation
// so there might occur some errors
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"
// Used for basic timing
#include <chrono>
#include <ctime>
class Timer
{
    std::chrono::high_resolution_clock::time_point mTimeStart;

public:
    Timer() { reset(); }
    long long stop()
    {
        std::chrono::high_resolution_clock::time_point timeEnd =
            std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                     mTimeStart)
            .count();
    }
    void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};
#endif

using namespace adf;

simpleGraph mygraph;

int main(int argc, char **argv)
{
    std::cout << "----- Vectorsum GMIO window -----" << std::endl;

    const int numOfTests = 9;
    const int numSamples[numOfTests] = {2048,
                                        4096,
                                        8192,
                                        16384,
                                        32768,
                                        1048576,
                                        2097152,
                                        20971520,
                                        41943040};

// To use GMIO in the Hardware  flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
    // Create XRT device handle for ADF API
    char *xclbinFilename = argv[1];
    auto dhdl = xrtDeviceOpen(0); // device index=0
    xrtDeviceLoadXclbinFile(dhdl, xclbinFilename);
    xuid_t uuid;
    xrtDeviceGetXclbinUUID(dhdl, uuid);
    adf::registerXRT(dhdl, uuid);

#endif
    auto throughputs = new float[2][numOfTests];
    adf::return_code ret;

    for (int i = 0; i < numOfTests; i++)
    {
        int n = numSamples[i];
        std::cout << "Running Tests for " << n << " Samples" << std::endl;
        int NUM_SAMPLES_TOTAL = n;

        int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;

        int BLOCK_SIZE_in_Bytes = NUM_SAMPLES_TOTAL * sizeof(float);
        int BLOCK_SIZE_out_Bytes = NUM_SAMPLES_TOTAL * sizeof(float);

        // Starting graph
        mygraph.init();

        // GMIO Malloc all needed arrays
        float *xArray = (float *)GMIO::malloc(BLOCK_SIZE_in_Bytes);
        float *yArray = (float *)GMIO::malloc(BLOCK_SIZE_in_Bytes);
        float *zArray = (float *)GMIO::malloc(BLOCK_SIZE_in_Bytes);
        float *zGolden = new float[NUM_SAMPLES_TOTAL];

        // Init data arrays
        for (int i = 0; i < NUM_SAMPLES_TOTAL; i++)
        {
            xArray[i] = i;
            yArray[i] = i;
            zGolden[i] = i + i;
        }
        int error = 0;

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
        const int WINDOW_SIZE_in_bytes = NUM_SAMPLES * sizeof(float); // Number of samples per iteration * sizeof(float)
        event::handle handle = event::start_profiling(mygraph.out, event::io_stream_start_to_bytes_transferred_cycles, WINDOW_SIZE_in_bytes * ITERATIONS);
        if (handle == event::invalid_handle)
        {
            printf("ERROR:Invalid handle. Only two performance counter in a AIE-PL interface tile\n");
            return 1;
        }

        Timer timer;
#endif
        // Timed part begin

        // connect the gmios of the graph with the arrays
        mygraph.in_x.gm2aie_nb(xArray, BLOCK_SIZE_in_Bytes);
        mygraph.in_y.gm2aie_nb(yArray, BLOCK_SIZE_in_Bytes);

        // actually run the graph
        ret = mygraph.run(ITERATIONS);
        if (ret != adf::ok)
        {
            printf("Run failed\n");
            return ret;
        }

        // get the ouput from the aie
        mygraph.out.aie2gm_nb(zArray, BLOCK_SIZE_in_Bytes);
        mygraph.out.wait();
        mygraph.wait();

        // Timed part end
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)

        double timer_stop = timer.stop();
        double throughputTimer = ((double)WINDOW_SIZE_in_bytes * ITERATIONS) / timer_stop * 1000000 / 1024 / 1024; // Mbytes per second
                                                                                                                   // Read performance counter value immediately
        // Assuming that overhead can be negligible if iteration is large enough
        long long cycle_count = event::read_profiling(handle);
        double throughputEventAPI = (double)WINDOW_SIZE_in_bytes * ITERATIONS / (1024 * 1024) / (cycle_count * 1e-9); // bytes per second
        throughputs[0][i] = throughputTimer;
        throughputs[1][i] = throughputEventAPI;

        event::stop_profiling(handle); // Performance counter is released and cleared
#endif
        ret = mygraph.end();

        int errorCount = 0;
        // Verification of the results
        for (int j = 0; j < NUM_SAMPLES_TOTAL; j++)
        {
            if (zArray[j] != zGolden[j])
            {
                std::cout << "ERROR:dout[" << j << "]=" << zArray[j] << ",gold=" << zGolden[j] << std::endl;
                errorCount++;
            }
        }
        if (errorCount)
            printf("Test failed with %d errors\n", errorCount);
        else
            printf("TEST PASSED\n");

        // finishing up
        GMIO::free(xArray);
        GMIO::free(yArray);
        GMIO::free(zArray);
        delete[] zGolden;
    }
    std::cout << "Throughput in MB/s:" << std::endl;
    std::cout << "NumOfSamples      Timer  Event-API" << std::endl;
    for (int i = 0; i < numOfTests; i++)
    {
        printf("%12.i %10.3f %10.3f\n", numSamples[i], throughputs[0][i], throughputs[1][i]);
    }
    if (ret != adf::ok)
    {
        printf("End failed\n");
        return ret;
    }
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
    xrtDeviceClose(dhdl);
#endif

    return 0;
}
