#include "graph.cpp"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fstream>

/***********************************************************************************\
 *  This programm is used for sw_emulation, hw_emulation and hw_execution.          *
 *  It:                                                                             *
 *  - allocates memory that is needed for the input/output of the kernels           *
 *  - arranges the data transfer with the AI Engines (in this case PLIO is used)    *
 *  - checks that the returned data checks out with the expected results            *
 *  In this case it is also used to do some timings on the program                  *
 \*********************************************************************************/

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
// This is used for the PL Kernels
#include "xrt.h"
#include "experimental/xrt_kernel.h"
#include <chrono>
#include <ctime>
class Timer
{
    std::chrono::high_resolution_clock::time_point mTimeStart;

public:
    Timer() { reset(); }
    long long stop()
    {
        std::chrono::high_resolution_clock::time_point timeEnd =
            std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                     mTimeStart)
            .count();
    }
    void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};
#endif

// Using the ADF API that call XRT API
#include "adf/adf_api/XRTConfig.h"

static std::vector<char>
load_xclbin(xrtDeviceHandle device, const std::string &fnm)
{
    if (fnm.empty())
        throw std::runtime_error("No xclbin specified");

    // load bit stream
    std::ifstream stream(fnm);
    stream.seekg(0, stream.end);
    size_t size = stream.tellg();
    stream.seekg(0, stream.beg);

    std::vector<char> header(size);
    stream.read(header.data(), size);

    auto top = reinterpret_cast<const axlf *>(header.data());
    if (xrtDeviceLoadXclbin(device, top))
        throw std::runtime_error("Xclbin loading failed");

    return header;
}

// Number of samples expected of the Kernel per run, defined in kernel.h (NUM_SAMPLES = 2048)
int main(int argc, char **argv)
{
    std::cout << "----- Vectorsum PLIO stream -----" << std::endl;

    const int numOfTests = 9;
    const int numSamples[numOfTests] = {2048,
                                        4096,
                                        8192,
                                        16384,
                                        32768,
                                        1048576,
                                        2097152,
                                        20971520,
                                        41943040};

    //////////////////////////////////////////
    // Open xclbin
    //////////////////////////////////////////

    auto dhdl = xrtDeviceOpen(0); // Open Device the local device
    if (dhdl == nullptr)
        throw std::runtime_error("No valid device handle found. Make sure using right xclOpen index.");
    auto xclbin = load_xclbin(dhdl, "a.xclbin");
    auto top = reinterpret_cast<const axlf *>(xclbin.data());
    adf::registerXRT(dhdl, top->m_header.uuid);

    auto throughputs = new float[2][numOfTests];

    // Runs for multiple different amounts of samples
    for (int i = 0; i < numOfTests; i++)
    {
        int n = numSamples[i];
        std::cout << "Running Tests for " << n << " Samples" << std::endl;
        // Total amount of samples that should be operated on
        int NUM_SAMPLES_TOTAL = n;

        int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;

        int sizeIn = NUM_SAMPLES_TOTAL;
        int sizeOut = NUM_SAMPLES_TOTAL;
        float *outGolden = new float[NUM_SAMPLES_TOTAL];

        //////////////////////////////////////////
        // input memory
        // Allocating the input size of sizeIn to MM2S
        // This is using low-level XRT call xclAllocBO to allocate the memory
        //////////////////////////////////////////

        xrtBufferHandle in_xbohdl = xrtBOAlloc(dhdl, sizeIn * sizeof(float), 0, 0);
        xrtBufferHandle in_ybohdl = xrtBOAlloc(dhdl, sizeIn * sizeof(float), 0, 0);
        auto in_xbomapped = reinterpret_cast<float *>(xrtBOMap(in_xbohdl));
        auto in_ybomapped = reinterpret_cast<float *>(xrtBOMap(in_ybohdl));

        std::cout << "Filling Arrays" << std::endl;
        // fill in the data
        for (int i = 0; i < NUM_SAMPLES_TOTAL; i++)
        {
            in_xbomapped[i] = i;
            in_ybomapped[i] = i;
            outGolden[i] = i + i;
        }

#if defined(__SYNCBO_ENABLE__)
        xrtBOSync(in_xbohdl, XCL_BO_SYNC_BO_TO_DEVICE, sizeIn * sizeof(float), 0);
        xrtBOSync(in_ybohdl, XCL_BO_SYNC_BO_TO_DEVICE, sizeIn * sizeof(float), 0);
#endif

        //////////////////////////////////////////
        // output memory
        // Allocating the output size of sizeOut to S2MM
        // This is using low-level XRT call xclAllocBO to allocate the memory
        //////////////////////////////////////////

        xrtBufferHandle out_bohdl = xrtBOAlloc(dhdl, sizeOut * sizeof(float), 0, 0);
        auto out_bomapped = reinterpret_cast<float *>(xrtBOMap(out_bohdl));
        memset(out_bomapped, 0, sizeOut * sizeof(float));

        //////////////////////////////////////////
        // mm2s ip
        // Using the xrtPLKernelOpen function to manually control the PL Kernel
        // that is outside of the AI Engine graph
        //////////////////////////////////////////

        xrtKernelHandle mm2s_xkhdl = xrtPLKernelOpen(dhdl, top->m_header.uuid, "mm2s:{mm2sx}");
        xrtKernelHandle mm2s_ykhdl = xrtPLKernelOpen(dhdl, top->m_header.uuid, "mm2s:{mm2sy}");
        // Need to provide the kernel handle, and the argument order of the kernel arguments
        // Here the in_bohdl is the input buffer, the nullptr is the streaming interface and must be null,
        // lastly, the size of the data. This info can be found in the kernel definition.

        //////////////////////////////////////////
        // s2mm ip
        // Using the xrtPLKernelOpen function to manually control the PL Kernel
        // that is outside of the AI Engine graph
        //////////////////////////////////////////

        xrtKernelHandle s2mm_khdl = xrtPLKernelOpen(dhdl, top->m_header.uuid, "s2mm");
        // Need to provide the kernel handle, and the argument order of the kernel arguments
        // Here the out_bohdl is the output buffer, the nullptr is the streaming interface and must be null,
        // lastly, the size of the data. This info can be found in the kernel definition.

        // printf("run s2mm\n");

        //////////////////////////////////////////
        // graph execution for AIE
        //////////////////////////////////////////

        // printf("graph init. This does nothing because CDO in boot PDI already configures AIE.\n");
        mygraph.init();

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
        const int WINDOW_SIZE_in_bytes = 8192;

        // This measures the number of samples sent to in_x PLIO input of the kernel
        event::handle handle = event::start_profiling(mygraph.out, event::io_stream_start_to_bytes_transferred_cycles, WINDOW_SIZE_in_bytes * ITERATIONS);
        if (handle == event::invalid_handle)
        {
            printf("ERROR: Invalid handle. Only two performance counter in a AIE-PL interface tile\n");
            return 1;
        }

        Timer timer;
#endif
        // Timed part Begin

        xrtRunHandle mm2s_xrhdl = xrtKernelRun(mm2s_xkhdl, in_xbohdl, nullptr, sizeIn);
        xrtRunHandle mm2s_yrhdl = xrtKernelRun(mm2s_ykhdl, in_ybohdl, nullptr, sizeIn);
        auto s2mm_rhdl = xrtKernelRun(s2mm_khdl, out_bohdl, nullptr, sizeOut);
        // printf("graph run\n");
        mygraph.run(ITERATIONS);
        mygraph.wait();

        // Timing End
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
        xrtRunWait(s2mm_rhdl); // performance counter 0 stops, assumming s2mm able to receive all data

        double timer_stop = timer.stop();
        double throughputTimer = ((double)WINDOW_SIZE_in_bytes * ITERATIONS) / timer_stop * 1000000 / 1024 / 1024; // Mbytes per second
        // throughputmean += throughput / 10.;
        // std::cout << "Throughput (by timer in 1,out 1):" << throughput << "M Bytes/s" << std::endl;
        long long cycle_count = event::read_profiling(handle);
        // printf("Cycle count: %lld\n", cycle_count);
        double throughputEventAPI = (double)WINDOW_SIZE_in_bytes * ITERATIONS / (cycle_count * 1e-9) / 1024 / 1024; // Mbytes per second
        throughputs[0][i] = throughputTimer;                                                                        // printf("Throughput: %f Mbytes/sec\n", throughput);
        throughputs[1][i] = throughputEventAPI;                                                                     // printf("Latency cycles=: %lld\n", cycle_count);
                                                                                                                    // double bandwidth = (double)(WINDOW_SIZE_in_bytes * iterations / 4) / cycle_count;
                                                                                                                    // printf("Bandwidth: %f \n", bandwidth);
        event::stop_profiling(handle);                                                                              // Performance counter is released and cleared
#endif

        mygraph.end();
        // printf("graph end\n");

        //////////////////////////////////////////
        // wait for mm2s done
        //////////////////////////////////////////

        auto state = xrtRunWait(mm2s_xrhdl);
        // std::cout << "mm2sx completed with status(" << state << ")\n";
        state = xrtRunWait(mm2s_yrhdl);
        // std::cout << "mm2sy completed with status(" << state << ")\n";
        xrtRunClose(mm2s_xrhdl);
        xrtRunClose(mm2s_yrhdl);
        xrtKernelClose(mm2s_xkhdl);
        xrtKernelClose(mm2s_ykhdl);

        //////////////////////////////////////////
        // wait for s2mm done
        //////////////////////////////////////////

        state = xrtRunWait(s2mm_rhdl);
        // std::cout << "s2mm completed with status(" << state << ")\n";
        xrtRunClose(s2mm_rhdl);
        xrtKernelClose(s2mm_khdl);

#if defined(__SYNCBO_ENABLE__)
        xrtBOSync(out_bohdl, XCL_BO_SYNC_BO_FROM_DEVICE, sizeOut * sizeof(float), 0);
#endif

        //////////////////////////////////////////
        // Comparing the execution data to the golden data
        //////////////////////////////////////////

        int errorCount = 0;

        for (int i = 0; i < sizeOut; i++)
        {
            if ((float)out_bomapped[i] != (float)outGolden[i])
            {
                printf("Error found @ %d, %f != %f\n", i, (float)out_bomapped[i], (float)outGolden[i]);
                errorCount++;
            }
        }

        if (errorCount)
            printf("Test failed with %d errors\n", errorCount);
        else
            printf("TEST PASSED\n");

        //////////////////////////////////////////
        // clean up XRT
        //////////////////////////////////////////

        // std::cout << "Releasing remaining XRT objects...\n";
        // xrtBOUnmap(dhdl, in_bohdl, in_bomapped);
        // xrtBOUnmap(dhdl, out_bohdl, out_bomapped);
        xrtBOFree(in_xbohdl);
        xrtBOFree(in_ybohdl);
        xrtBOFree(out_bohdl);
        delete[] outGolden;
    }
    std::cout << "Throughput in MB/s:" << std::endl;
    std::cout << "NumOfSamples      Timer  Event-API" << std::endl;
    for (int i = 0; i < numOfTests; i++)
    {
        printf("%12.i %10.3f %10.3f\n", numSamples[i], throughputs[0][i], throughputs[1][i]);
    }
    // printf("Throughput: %f Mbytes/sec\n", throughputmean);
    // std::cout << "Releasing remaining XRT objects...\n";
    xrtDeviceClose(dhdl);
    return 0;
}
