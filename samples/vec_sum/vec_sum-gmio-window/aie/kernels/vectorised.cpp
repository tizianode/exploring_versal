#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "vectorised.h"

// Real floating point operations are on 8 lanes in parallel
#define VECTOR_LEN 8

/*********************************************************************************\
* Vectorised kernel to sum two vectors x and y                                    *
* This kernel receives and sends its data via windows.                            *
* Also the computation of the vector sum has been vectorised with AIE Intrinsics. *
\*********************************************************************************/

void vecsum_kernel(input_window<float> *in_x, input_window<float> *in_y, output_window<float> *out)
{
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Kernel started!!\n");
#endif
  v8float x = undef_v8float();
  v8float y = undef_v8float();

  v8float z;

  const unsigned LSIZE = (NUM_SAMPLES / VECTOR_LEN);

  for (unsigned i = 0; i < LSIZE; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_readincr(in_x, x);
      window_readincr(in_y, y);

      z = fpadd(x, y);

      window_writeincr(out, z);
    }
}
