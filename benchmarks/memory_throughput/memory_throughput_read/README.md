# DDR memory read throughput benchmarking from DDR to AI-Engines

This is a small program that uses the different methods of data access between the DDR Memory of the board and the AI-Engines.  
The options tested include the following:
- GMIO and window/streams
- PLIO and window/streams (with PLIO widths of 32/64/128 bits)
  
For instructions on how to run the benchmark see `../README.md` 

The resulting `host.exe` will then run for the specified amount of iterations (`host.cpp`: numOfRuns) and sizes of chunks of data (`host.cpp`: numSamples[] and numOfTests). It automatically generates some .csv files with pairs of chunksize and measured throughput.
