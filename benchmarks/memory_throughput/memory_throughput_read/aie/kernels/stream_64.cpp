#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "stream_64.h"

/*
  Kernel to measure off chip memory read bandwidth using streams
  This kernel is set up to work with 64 bit width PLIO connections
  (this is the reason why we writeincr out twice, then we have 64 bit)

  As you can see from the code, all stream based kernels are run for a constant amount of samples.
  While we are specifing this number of samples at compile time (see NUM_SAMPLES in stream_64.h),
  this could also be implemented using a runtime parameter. Then it would be possible to change the amount during runtime.
*/

void kernel_stream_64(input_stream<int> *in, output_stream<int> *out)
{
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Started kernel stream 64\n");
#endif

  int x = 0;
  int z = 0;

  for (int i = 0; i < NUM_SAMPLES; i++)
  {
    x = readincr(in);
    z += x;
  }
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Writing output... z: %d\n", z);
#endif
  writeincr(out, z);
  writeincr(out, 0);
}
