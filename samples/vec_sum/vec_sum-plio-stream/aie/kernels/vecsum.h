#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H

#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"

#define NUM_SAMPLES 2048 // length of vector x and y

/*
	Kernel to sum two vectors x and y
*/
void vecsum_kernel(input_stream_float *in_x, input_stream_float *in_y, output_stream_float *out);

#endif
