/*******************************************************************************\
* This host program can be used to benchmark the throughput                     *
* between the DDR Memory of the board and multiple AIEs (i.e. memory read) .    *
* "nAIE" is defined and can be changed in graph.h.                               *
* This was here only implemented for GMIO based data transfer in combination    *
* with windows.                                                                 *
* The variables "numOfRuns", "numOfTests" and "numSamples" are used to define:   *
* "numOfRuns": How often should the benchmark be run                            *
* "numOfTests": until what index of the numSamples array should be tested       *
* "numSamples": an array containing different number of samples that will be    *
*               sent/read                                                       *
* Note that the maximum allocatable memory is bound by the CMA of the OS, in    *
* this case 1 GB (see line 93)                                                  *
* The throughput results of the benchmark are then written to a csv file which   *
* name is defined in line 75.                                                    *
\*******************************************************************************/

#include "../aie/graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>
#include <iostream>
#include <string>
#include <algorithm>

// To use GMIO in Hardware flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
// This is used for the PL Kernels
#include "xrt.h"
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"

// Used for basic timing
#include <chrono>
#include <ctime>
class Timer
{
    std::chrono::high_resolution_clock::time_point mTimeStart;

public:
    Timer() { reset(); }
    long long stop()
    {
        std::chrono::high_resolution_clock::time_point timeEnd =
            std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                     mTimeStart)
            .count();
    }
    void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};
#endif

using namespace adf;

// Set the number of Iterations and number of Tests to be done
const int numOfRuns = 1;
const int numOfTests = 5;
const int numSamples[5] = {
    4194304,  // 16 MB
    8388608,  // 32 MB
    16777216, // 64 MB
    33554432, // 128 MB
    67108864, // 256 MB
};

int benchmark_gmio()
{
    windowGraphGMIO mygraph;

    auto throughputs = new float[numOfTests];
    adf::return_code ret;

    // Automatic creation of a csv file for analysis
    std::string csvName = "GMIO-window-multiple-in-nAIE" + std::to_string(nAIE) + ".csv";
    std::ofstream ResultFile(csvName);
    ResultFile << "nAIE,DataSent,Throughput\n";

    // Starting graph
    mygraph.init();
    // Number of iterations to be executed
    for (int j = 0; j < numOfRuns; j++)
    {
        // until what size of samples
        for (int i = 0; i < numOfTests; i++)
        {
            int n = numSamples[i];
            if (n % NUM_SAMPLES != 0)
            {
                std::cout << n << " is not multiple of " << NUM_SAMPLES << "! " << n % NUM_SAMPLES << std::endl;
                return -1;
            }
            if (n * nAIE * 4 >= 1073741824)
                break;

            std::cout << "Iteration " << j << ": Running Tests for " << n << " Samples and " << nAIE << " AIEs" << std::endl;
            int NUM_SAMPLES_TOTAL = n;

            int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;

            int BLOCK_SIZE_in_Bytes = NUM_SAMPLES_TOTAL * sizeof(int);
            int BLOCK_SIZE_out_Bytes = ITERATIONS * 2 * sizeof(int);

            // GMIO Malloc all needed arrays
            int *inArray[nAIE];
            int *outArray[nAIE];
            int *outGolden = new int[ITERATIONS * 2];

            for (int k = 0; k < nAIE; k++)
            {
                inArray[k] = (int *)GMIO::malloc(BLOCK_SIZE_in_Bytes);
                outArray[k] = (int *)GMIO::malloc(BLOCK_SIZE_out_Bytes);
            }

            // Init data arrays
            for (int k = 0; k < nAIE; k++)
                for (int i = 0; i < NUM_SAMPLES_TOTAL; i++)
                {
                    inArray[k][i] = 1;
                }

            // Init reference result data
            for (int i = 0; i < ITERATIONS * 2; i += 2)
            {
                outGolden[i] = NUM_SAMPLES;
                outGolden[i + 1] = 0;
            }

            // run the graph
            ret = mygraph.run(ITERATIONS);

            if (ret != adf::ok)
            {
                printf("Run failed\n");
                return -1;
            }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
            const int WINDOW_SIZE_in_bytes = NUM_SAMPLES * sizeof(int); // Number of samples per iteration * sizeof(int)
            Timer timer;
#endif
            // Timed part begin

            // connect the gmios of the graph with the arrays
            for (int k = 0; k < nAIE; k++)
                mygraph.in_window_gmio[k].gm2aie_nb(inArray[k], BLOCK_SIZE_in_Bytes);

            // get the ouput from the aies
            for (int k = 0; k < nAIE; k++)
                mygraph.out_window_gmio[k].aie2gm_nb(outArray[k], BLOCK_SIZE_out_Bytes);

            // wait for the outputs to finish
            for (int k = 0; k < nAIE; k++)
                mygraph.out_window_gmio[k].wait();

            // wait for the graph to finish
            mygraph.wait();

            // Timed part end
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)

            double timer_stop = timer.stop();
            double throughputTimer = ((double)WINDOW_SIZE_in_bytes * nAIE * ITERATIONS) / 1024 / 1024 / timer_stop * 1000000; // Mbytes per second
            // Assuming that overhead can be negligible if iteration is large enough
            throughputs[i] = throughputTimer;
#endif
            int errorCount = 0;
            // Verification of the results
            for (int k = 0; k < nAIE; k++)
                for (int j = 0; j < ITERATIONS * 2; j++)
                {
                    if (outArray[k][j] != outGolden[j])
                    {
                        std::cout << "ERROR:dout[" << j << "]=" << outArray[k][j] << ",gold=" << outGolden[j] << std::endl;
                        errorCount++;
                    }
                }
            if (errorCount)
                printf("Test failed with %d errors\n", errorCount);

            // finishing up
            for (int k = 0; k < nAIE; k++)
            {
                GMIO::free(inArray[k]);
                GMIO::free(outArray[k]);
            }
            delete[] outGolden;
        }

        // Saving the results
        for (int i = 0; i < numOfTests; i++)
        {
            ResultFile << nAIE << "," << (numSamples[i] * 4 / 1024 > 1024 ? std::to_string(numSamples[i] * 4 / 1024 / 1024) + " MB" : std::to_string(numSamples[i] * 4 / 1024) + " KB") << "," << throughputs[i] << "\n";
        }
    }
    ResultFile.close();
    // Close the graph
    ret = mygraph.end();

    if (ret != adf::ok)
    {
        printf("End failed\n");
        return -1;
    }
    return 1;
}

static std::vector<char>
load_xclbin(xrtDeviceHandle device, const std::string &fnm)
{
    if (fnm.empty())
        throw std::runtime_error("No xclbin specified");

    // load bit stream
    std::ifstream stream(fnm);
    stream.seekg(0, stream.end);
    size_t size = stream.tellg();
    stream.seekg(0, stream.beg);

    std::vector<char> header(size);
    stream.read(header.data(), size);

    auto top = reinterpret_cast<const axlf *>(header.data());
    if (xrtDeviceLoadXclbin(device, top))
        throw std::runtime_error("Xclbin loading failed");

    return header;
}

int main(int argc, char **argv)
{
    std::cout << "----- Benchmarks -----" << std::endl;

// To use GMIO in the Hardware  flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
    // Create XRT device handle for ADF API
    char *xclbinFilename = argv[1];
    auto dhdl = xrtDeviceOpen(0); // device index=0
    auto xclbin = load_xclbin(dhdl, xclbinFilename);
    auto top = reinterpret_cast<const axlf *>(xclbin.data());

    adf::registerXRT(dhdl, top->m_header.uuid);
#endif
    std::cout << "Benchmarking GMIO Window: " << std::endl;
    benchmark_gmio();

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
    xrtDeviceClose(dhdl);
#endif

    return 0;
}
