/***************************************************************************\
* This programm is used for x86simulation and aiesimulation.                *
* It can be used to check, that:                                            *
* - the graph is set up correctly                                           *
* - it is computing the correct output                                      *
\***************************************************************************/

#include "graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>

using namespace adf;

// instantiate graph
windowGraphPLIO wGraphPLIO;

const int numOfTests = 3;
const int numSamples[16] = {
    2048,     // 8 KB
    4096,     // 16 KB
    8192,     // 32 KB
    16384,    // 64 KB
    32768,    // 128 KB
    65536,    // 256 KB
    131072,   // 512 KB
    262144,   // 1 MB
    524288,   // 2 MB
    1048576,  // 4 MB
    2097152,  // 8 MB
    4194304,  // 16 MB
    8388608,  // 32 MB
    16777216, // 64 MB
    33554432, // 128 MB
    67108864, // 256 MB
};

// function to run PLIO graph
int benchmark_plio(graph mygraph)
{
    mygraph.init();

    mygraph.run(1);

    mygraph.end();

    return 0;
}

int main(int argc, char **argv)
{
    std::cout << "----- Memory throughput benchmark -----" << std::endl;
    std::cout << "Benchmarking PLIO window: " << std::endl;
    benchmark_plio(wGraphPLIO);
    return 0;
}
