#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "stream.h"

/*
  Kernel to measure off chip memory read bandwidth using streams
  This kernel is set up to work with 32 bit width PLIO streams
  (the reason why we have to send at least 64 bit in this stream kernel is unknown and not mentioned
  in the manufacturer documentation, but with only one 32-bit stream the kernel stalls)

  As you can see from the code, all stream based kernels are run for a constant amount of samples.
  While we are specifing this number of samples at compile time (see NUM_SAMPLES in stream.h),
  this could also be implemented using a runtime parameter. Then it would be possible to change the amount during runtime.
*/

void kernel_stream(input_stream<int> *in, output_stream<int> *out)
{
  int x = 0, z = 0;
  for (int i = 0; i < NUM_SAMPLES; i++)
  {
    x = readincr(in);
    z += x;
  }
  writeincr(out, z);
  writeincr(out, 0);
}
