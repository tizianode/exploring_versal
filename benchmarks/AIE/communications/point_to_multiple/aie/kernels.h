#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#define WINDOW_SIZE 8192
void sender(output_window<int32> *out);
void receiver(input_window<int32> *in);

#endif
