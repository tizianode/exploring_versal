/*******************************************************************************\
* This host program can be used to benchmark the throughput between an AIE      *
* and the DDR Memory of the board (i.e. memory write).                          *
* Possible combinations are:                                                    *
* - GMIO and windows/streams                                                    *
* - PLIO and windows/streams with a bit width of either 32,64 or 128 bit        *
* The variables "numOfRuns", "numOfTests" and "numSamples" are used to define:   *
* "numOfRuns": How often should the benchmark be run                            *
* "numOfTests": until what index of the numSamples array should be tested       *
* "numSamples": an array containing different number of samples that will be    *
*               sent/read                                                       *
* Note that the maximum allocatable memory is bound by the CMA of the OS, in    *
* this case 1 GB (see line 118)                                                 *
* The throughput results of the benchmark are then written to a csv file which   *
* name is defined in line 92 for GMIO and 199 for PLIO.                          *
\*******************************************************************************/

#include "../aie/graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>
#include <iostream>
#include <string>

// To use GMIO in Hardware flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
// This is used for the PL Kernels
#include "xrt.h"
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"

// Used for basic timing
#include <chrono>
#include <ctime>
class Timer
{
    std::chrono::high_resolution_clock::time_point mTimeStart;

public:
    Timer() { reset(); }
    long long stop()
    {
        std::chrono::high_resolution_clock::time_point timeEnd =
            std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                     mTimeStart)
            .count();
    }
    void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};
#endif

using namespace adf;

// This sets up the graphs and hardware for all the different combinations of GMIO and PLIO with windows and streams.
// All of the graphs will be present in the hardware at the same time. In this case this is not a problem because we
// evaluate them one by one and our program is so small that there are enough interfaces and connections for everything.windowGraphGMIO wGraphGMIO;
streamGraphGMIO sGraphGMIO;
windowGraphPLIO wGraphPLIO;
streamGraphPLIO sGraphPLIO;
windowGraphPLIO64 wGraphPLIO64;
streamGraphPLIO64 sGraphPLIO64;
windowGraphPLIO128 wGraphPLIO128;
streamGraphPLIO128 sGraphPLIO128;

const int numOfRuns = 1;
const int numOfTests = 15;
const int numSamples[15] = {
    4096,     // 16 KB
    8192,     // 32 KB
    16384,    // 64 KB
    32768,    // 128 KB
    65536,    // 256 KB
    131072,   // 512 KB
    262144,   // 1 MB
    524288,   // 2 MB
    1048576,  // 4 MB
    2097152,  // 8 MB
    4194304,  // 16 MB
    8388608,  // 32 MB
    16777216, // 64 MB
    33554432, // 128 MB
    67108864, // 256 MB
};

// function specifcally for the benchmarking of GMIO based connections
int benchmark_gmio(output_gmio out_port, graph mygraph, std::string typ)
{
    auto throughputs = new float[numOfTests];
    adf::return_code ret;
    // Automatic creation of a csv file for analysis
    std::string csvName = "GMIO-" + typ + ".csv";
    std::ofstream ResultFile(csvName);
    ResultFile << "DataSent,Throughput\n";

    // Starting graph
    mygraph.init();
    // Number of iterations to be executed
    for (int j = 0; j < numOfRuns; j++)
    {
        // until what size of samples
        for (int i = 0; i < numOfTests; i++)
        {
            int n = numSamples[i];
            if (n % NUM_SAMPLES != 0)
            {
                std::cout << n << " is not multiple of " << NUM_SAMPLES << "! " << n % NUM_SAMPLES << std::endl;
                return -1;
            }

            std::cout << "Iteration " << j << ": Running Tests for " << n << " Samples" << std::endl;

            int NUM_SAMPLES_TOTAL = n;                                  // Number of Data samples that is sent in total
            int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;           // Number of Iterations the kernels have to run
            int BLOCK_SIZE_out_Bytes = NUM_SAMPLES_TOTAL * sizeof(int); // Amount of Bytes we need to reserve in the off-chip memory to store the data

            // if amount of needed memory is bigger than CMA, break
            if (BLOCK_SIZE_out_Bytes >= 1073741824)
                break;

            // GMIO Malloc all needed arrays
            int *outArray = (int *)GMIO::malloc(BLOCK_SIZE_out_Bytes);
            int *outGolden = new int[NUM_SAMPLES_TOTAL];

            // Init data arrays
            for (int i = 0; i < NUM_SAMPLES_TOTAL; i++)
            {
                outGolden[i] = 1;
            }

            // run the graph
            ret = mygraph.run(ITERATIONS);

            if (ret != adf::ok)
            {
                printf("Run failed\n");
                return -1;
            }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
            const int WINDOW_SIZE_in_bytes = NUM_SAMPLES * sizeof(int); // Number of samples per iteration * sizeof(int)
            Timer timer;
#endif
            // Timed part begin
            // get the ouput from the aie
            out_port.aie2gm_nb(outArray, BLOCK_SIZE_out_Bytes);
            // wait for the memory transaction to finsih
            out_port.wait();
            // wait for the graph to finish
            mygraph.wait();

            // Timed part end
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
            double timer_stop = timer.stop();
            double throughputTimer = ((double)WINDOW_SIZE_in_bytes * ITERATIONS) / 1024 / 1024 / timer_stop * 1000000; // Mbytes per second
            // Assuming that overhead can be negligible if iteration is large enough
            throughputs[i] = throughputTimer;
#endif
            int errorCount = 0;
            // Verification of the results
            for (int j = 0; j < NUM_SAMPLES_TOTAL; j++)
            {
                if (outArray[j] != outGolden[j])
                {
                    std::cout << "ERROR:dout[" << j << "]=" << outArray[j] << ",gold=" << outGolden[j] << std::endl;
                    errorCount++;
                }
            }
            if (errorCount)
                printf("Test failed with %d errors\n", errorCount);

            // finishing up
            GMIO::free(outArray);
            delete[] outGolden;
        }

        // Save the results
        for (int i = 0; i < numOfTests; i++)
        {
            ResultFile << (numSamples[i] * 4 / 1024 > 1024 ? std::to_string(numSamples[i] * 4 / 1024 / 1024) + " MB" : std::to_string(numSamples[i] * 4 / 1024) + " KB") << "," << throughputs[i] << "\n";
        }
    }
    // close graph
    ret = mygraph.end();

    ResultFile.close();
    if (ret != adf::ok)
    {
        printf("End failed\n");
        return -1;
    }
    return 1;
}

// function used specifically for PLIO typed benchmarks
int benchmark_plio(xrtDeviceHandle dhdl, const axlf *top, output_plio out_port, graph mygraph, std::string typ, int width)
{
    // automatic generation of a csv file for analysis
    std::string csvName = "PLIO-" + typ + "-" + std::to_string(width) + "bit.csv";
    std::ofstream ResultFile(csvName);
    ResultFile << "DataSent,Throughput\n";
    adf::return_code ret;

    // Starting graph
    mygraph.init();
    for (int j = 0; j < numOfRuns; j++)
    {
        auto throughputs = new float[numOfTests];

        for (int i = 0; i < numOfTests; i++)
        {
            int n = numSamples[i];
            if (n % NUM_SAMPLES != 0)
            {
                std::cout << n << " is not multiple of " << NUM_SAMPLES << "! " << n % NUM_SAMPLES << std::endl;
                return -1;
            }

            std::cout << "Iteration " << j << ":Running Tests for " << n << " Samples" << std::endl;

            int NUM_SAMPLES_TOTAL = n;                        // Number of Data samples that is sent in total
            int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES; // Number of Iterations the kernels have to run
            int sizeOut = NUM_SAMPLES_TOTAL;                  // Amount of Bytes we need to reserve in the off-chip memory to store the data

            //////////////////////////////////////////
            // input memory
            // Allocating the input size of sizeIn to MM2S
            // This is using low-level XRT call xclAllocBO to allocate the memory
            //////////////////////////////////////////

            int *outGolden = new int[sizeOut];
            // Init data arrays

            for (int i = 0; i < NUM_SAMPLES_TOTAL; i++)
            {
                outGolden[i] = 1;
            }

#if defined(__SYNCBO_ENABLE__)
            xrtBOSync(in_bohdl, XCL_BO_SYNC_BO_TO_DEVICE, sizeIn * sizeof(int), 0);
#endif
            //////////////////////////////////////////
            // output memory
            // Allocating the output size of sizeOut to S2MM
            // This is using low-level XRT call xclAllocBO to allocate the memory
            //////////////////////////////////////////

            xrtBufferHandle out_bohdl = xrtBOAlloc(dhdl, sizeOut * sizeof(int), 0, 0);
            auto out_bomapped = reinterpret_cast<int *>(xrtBOMap(out_bohdl));
            memset(out_bomapped, 0, sizeOut * sizeof(int));

            xrtKernelHandle s2mm_khdl;

            // difference between stream and window kernels and also between PLIO bit widths
            if (width == 32)
                if (typ.compare("stream") == 0)
                {
                    // Kernel handle
                    s2mm_khdl = xrtPLKernelOpen(dhdl, top->m_header.uuid, "s2mm:{s2mms}");
                }
                else
                {
                    // Kernel handle
                    s2mm_khdl = xrtPLKernelOpen(dhdl, top->m_header.uuid, "s2mm:{s2mmw}");
                }
            else if (width == 64)
                if (typ.compare("stream") == 0)
                {
                    // Kernel handle
                    s2mm_khdl = xrtPLKernelOpen(dhdl, top->m_header.uuid, "s2mm_64:{s2mms_64}");
                }
                else
                {
                    // Kernel handle
                    s2mm_khdl = xrtPLKernelOpen(dhdl, top->m_header.uuid, "s2mm_64:{s2mmw_64}");
                }
            else if (width == 128)
            {
                if (typ.compare("stream") == 0)
                {
                    // Kernel handle
                    s2mm_khdl = xrtPLKernelOpen(dhdl, top->m_header.uuid, "s2mm_128:{s2mms_128}");
                }
                else
                {
                    // Kernel handle
                    s2mm_khdl = xrtPLKernelOpen(dhdl, top->m_header.uuid, "s2mm_128:{s2mmw_128}");
                }
            }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
            const int WINDOW_SIZE_in_bytes = NUM_SAMPLES * sizeof(int); // Number of samples per iteration * sizeof(int)
            Timer timer;
#endif
            // Timed part begin
            // start the memory transaction
            xrtRunHandle s2mm_rhdl = xrtKernelRun(s2mm_khdl, out_bohdl, nullptr, sizeOut / (width / 32));

            // run the graph
            ret = mygraph.run(ITERATIONS);

            if (ret != adf::ok)
            {
                printf("Run failed\n");
                return -1;
            }
            // Wait for the graph to finish
            mygraph.wait();

            // Timed part end
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
            // wait for the memory transaction to finish
            xrtRunWait(s2mm_rhdl);

            double timer_stop = timer.stop();
            double throughputTimer = ((double)WINDOW_SIZE_in_bytes * ITERATIONS) / 1024 / 1024 / timer_stop * 1000000; // Mbytes per second
            throughputs[i] = throughputTimer;
#endif
            // close the PL kernel
            xrtRunWait(s2mm_rhdl);
            xrtRunClose(s2mm_rhdl);
            xrtKernelClose(s2mm_khdl);

#if defined(__SYNCBO_ENABLE__)
            xrtBOSync(out_bohdl, XCL_BO_SYNC_BO_FROM_DEVICE, sizeOut * sizeof(int), 0);
#endif
            int errorCount = 0;
            // Verification of the result
            for (int j = 0; j < sizeOut; j++)
            {
                if (out_bomapped[j] != outGolden[j])
                {
                    std::cout << "ERROR:dout[" << j << "]=" << out_bomapped[j] << ",gold=" << outGolden[j] << std::endl;
                    errorCount++;
                }
            }

            if (errorCount)
                printf("Test failed with %d errors\n", errorCount);

            // finishing up
            xrtBOFree(out_bohdl);
            delete[] outGolden;
        }
        // saving the results
        for (int i = 0; i < numOfTests; i++)
        {
            ResultFile << (numSamples[i] * 4 / 1024 > 1024 ? std::to_string(numSamples[i] * 4 / 1024 / 1024) + " MB" : std::to_string(numSamples[i] * 4 / 1024) + " KB") << "," << throughputs[i] << "\n";
        }
    }
    // end the graph
    mygraph.end();

    ResultFile.close();

    if (ret != adf::ok)
    {
        printf("End failed\n");
        return -1;
    }

    return 0;
}

static std::vector<char>
load_xclbin(xrtDeviceHandle device, const std::string &fnm)
{
    if (fnm.empty())
        throw std::runtime_error("No xclbin specified");

    // load bit stream
    std::ifstream stream(fnm);
    stream.seekg(0, stream.end);
    size_t size = stream.tellg();
    stream.seekg(0, stream.beg);

    std::vector<char> header(size);
    stream.read(header.data(), size);

    auto top = reinterpret_cast<const axlf *>(header.data());
    if (xrtDeviceLoadXclbin(device, top))
        throw std::runtime_error("Xclbin loading failed");

    return header;
}

int main(int argc, char **argv)
{
    std::cout << "----- Benchmarks -----" << std::endl;

// To use GMIO in the Hardware  flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
    // Create XRT device handle for ADF API
    char *xclbinFilename = argv[1];
    auto dhdl = xrtDeviceOpen(0); // device index=0
    auto xclbin = load_xclbin(dhdl, xclbinFilename);
    auto top = reinterpret_cast<const axlf *>(xclbin.data());

    adf::registerXRT(dhdl, top->m_header.uuid);
#endif

    std::string wString = "window";
    std::string sString = "stream";

    // Different possible Benchmarking function calls
    std::cout << "Benchmarking GMIO Stream: " << std::endl;
    benchmark_gmio(sGraphGMIO.out_stream_gmio, sGraphGMIO, sString);
    std::cout << "Benchmarking GMIO Window: " << std::endl;
    benchmark_gmio(wGraphGMIO.out_window_gmio, wGraphGMIO, wString);
    std::cout << "Benchmarking PLIO Stream 32bit: " << std::endl;
    benchmark_plio(dhdl, top, sGraphPLIO.out_stream_plio, sGraphPLIO, sString, 32);
    std::cout << "Benchmarking PLIO Window 32bit: " << std::endl;
    benchmark_plio(dhdl, top, wGraphPLIO.out_window_plio, wGraphPLIO, wString, 32);
    std::cout << "Benchmarking PLIO Stream 64bit: " << std::endl;
    benchmark_plio(dhdl, top, sGraphPLIO64.out_stream_plio_64, sGraphPLIO64, sString, 64);
    std::cout << "Benchmarking PLIO Window 64bit: " << std::endl;
    benchmark_plio(dhdl, top, wGraphPLIO64.out_window_plio_64, wGraphPLIO64, wString, 64);
    std::cout << "Benchmarking PLIO Stream 128bit: " << std::endl;
    benchmark_plio(dhdl, top, sGraphPLIO128.out_stream_plio_128, sGraphPLIO128, sString, 128);
    std::cout << "Benchmarking PLIO Window 128bit: " << std::endl;
    benchmark_plio(dhdl, top, wGraphPLIO128.out_window_plio_128, wGraphPLIO128, wString, 128);

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
    xrtDeviceClose(dhdl);
#endif

    return 0;
}
