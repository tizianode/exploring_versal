#include "graph.h"

simpleGraph mygraph;

// This main() function runs only for AIESIM and X86Sim targets.
// Emulation uses a different host code
#if defined(__AIESIM__) || defined(__X86SIM__)
int main(void)
{
  adf::return_code ret;
  mygraph.init();
  ret = mygraph.run(1);
  if (ret != adf::ok)
  {
    printf("Run failed\n");
    return ret;
  }
  ret = mygraph.end();
  if (ret != adf::ok)
  {
    printf("End failed\n");
    return ret;
  }
  return 0;
}
#endif