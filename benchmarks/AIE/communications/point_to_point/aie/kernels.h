#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#define WINDOW_SIZE 16384
#define NUM_SAMPLES 1048576
void sender(output_window<int32> *out, int64 size);
void receiver(input_window<int32> *in, int64 size);

#endif
