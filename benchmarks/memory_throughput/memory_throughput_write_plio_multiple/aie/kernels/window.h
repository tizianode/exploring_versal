#ifndef FUNCTION_WINDOW_H
#define FUNCTION_WINDOW_H

#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"

#define NUM_SAMPLES 2048 // Number of samples that are processed per kernel Iteration

/*
    Kernel that sends NUM_SAMPLES ones using windows
*/
void kernel_window(output_window<int> *out);

#endif
