#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "window_out.h"

/*
  Kernel to measure off chip memory read bandwidth using streams
  It just sends a vector of 1s for NUM_SAMPLES
*/
#define VECTOR_LEN 8

void kernel_window_out(output_window<int> *out)
{
  v8int32 xV = null_v8int32();
  for (int i = 0; i < VECTOR_LEN; i++)
  {
    xV = upd_elem(xV, i, 1);
  }
  for (int i = 0; i < NUM_SAMPLES / VECTOR_LEN; i++)
  {
    window_writeincr(out, xV);
  }
}
