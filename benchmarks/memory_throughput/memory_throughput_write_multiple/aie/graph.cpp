/***************************************************************************\
* This programm is used for x86simulation and aiesimulation.                *
* It can be used to check, that:                                            *
* - the graph combinations of GMIO/PLIO and windows/streams                 *
*   are set up correctly                                                    *
* - they are computing the correct output                                   *
\***************************************************************************/

#include "graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>

using namespace adf;

// Choose which version to use, both GMIO based
// windowGraphGMIO mygraph;
streamGraphGMIO mygraph;

const int numOfTests = 3;
const int numSamples[16] = {
    2048,     // 8 KB
    4096,     // 16 KB
    8192,     // 32 KB
    16384,    // 64 KB
    32768,    // 128 KB
    65536,    // 256 KB
    131072,   // 512 KB
    262144,   // 1 MB
    524288,   // 2 MB
    1048576,  // 4 MB
    2097152,  // 8 MB
    4194304,  // 16 MB
    8388608,  // 32 MB
    16777216, // 64 MB
    33554432, // 128 MB
    67108864, // 256 MB
};

int main(int argc, char **argv)
{
    std::cout << "----- Memory throughput benchmark -----" << std::endl;

    std::cout << "Benchmarking GMIO window: " << std::endl;
    // std::cout << "Benchmarking GMIO stream: " << std::endl;

    adf::return_code ret;

    // Starting graph
    mygraph.init();
    for (int i = 0; i < numOfTests; i++)
    {
        int n = numSamples[i];
        if (n % NUM_SAMPLES != 0)
        {
            std::cout << n << " is not multiple of" << NUM_SAMPLES << "! " << n % NUM_SAMPLES << std::endl;
            return -1;
        }
        // Stop if allocated memory would be bigger than CMA
        if (n * nAIE * 4 >= 1073741824)
            break;

        std::cout << "Running Tests for " << n << " Samples and " << nAIE << " AIEs." << std::endl;

        int NUM_SAMPLES_TOTAL = n;

        int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;

        int BLOCK_SIZE_out_Bytes = NUM_SAMPLES_TOTAL * sizeof(int);

        // GMIO Malloc all needed arrays
        int *outArray[nAIE];
        std::cout << "Allocating outarrays" << std::endl;

        for (int k = 0; k < nAIE; k++)
        {
            outArray[k] = (int *)GMIO::malloc(BLOCK_SIZE_out_Bytes);
        }

        // Fill reference array
        int *outGolden = new int[NUM_SAMPLES_TOTAL];
        for (int k = 0; k < NUM_SAMPLES_TOTAL; k++)
            outGolden[k] = 1;

        // run the graph
        ret = mygraph.run(ITERATIONS);

        if (ret != adf::ok)
        {
            printf("Run failed\n");
            return -1;
        }

        // start data transfers from the aie
        std::cout << "Sending data" << std::endl;
        for (int k = 0; k < nAIE; k++)
        {
            mygraph.out_stream_gmio[k].aie2gm_nb(outArray[k], BLOCK_SIZE_out_Bytes);
        }

        // wait for the data transfers to finish
        std::cout << "Waiting for data" << std::endl;
        for (int k = 0; k < nAIE; k++)
        {
            mygraph.out_stream_gmio[k].wait();
        }

        int errorCount = 0;
        // Verification of the results
        for (int k = 0; k < nAIE; k++)
            for (int j = 0; j < NUM_SAMPLES_TOTAL; j++)
            {
                if (outArray[k][j] != outGolden[j])
                {
                    std::cout << "ERROR:dout[" << j << "]=" << outArray[k][j] << ",gold=" << outGolden[j] << std::endl;
                    errorCount++;
                }
            }

        // Wait for graph to finish, before starting it again
        mygraph.wait();

        if (errorCount)
            printf("Test failed with %d errors\n", errorCount);
        else
            printf("TEST PASSED\n");

        // finishing up
        for (int k = 0; k < nAIE; k++)
            GMIO::free(outArray[k]);
        delete[] outGolden;
    }

    // close the graph
    ret = mygraph.end();
    if (ret != adf::ok)
    {
        printf("End failed\n");
        return -1;
    }
    return 0;
}
