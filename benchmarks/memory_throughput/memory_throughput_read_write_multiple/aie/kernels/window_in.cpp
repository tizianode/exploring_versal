#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "window_in.h"

/*
  Kernel to measure off chip memory read bandwidth using streams
  it sums up all the input numbers (vectorised) and sends back the results the host program for checks
*/
#define VECTOR_LEN 8

void kernel_window_in(input_window<int> *in, output_window<int> *out)
{
  v8int32 xV = null_v8int32();
  v8int32 zV = null_v8int32();
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Kernel started\n");
#endif

  const unsigned LSIZE = (NUM_SAMPLES / VECTOR_LEN);
  for (unsigned i = 0; i < LSIZE; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_readincr(in, xV);
      zV = operator+(xV, zV);
    }

  int z = 0;
  for (unsigned i = 0; i < VECTOR_LEN; i++)
  {
    z += ext_elem(zV, i);
  }
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Writing z: %d\n", z);
#endif
  window_writeincr(out, z);
  window_writeincr(out, 0);
}