This mini application is taken from the Versal ACAP AI Engine Programming Environment guide (Chapter 3).

It is composed by two identical kernels computing over a sequence of complex numbers. Being `c1` the input number and `c2` the output number:
```C++
    c2.real = c1.real+c1.imag;
    c2.imag = c1.real-c1.imag;
```



References:
- tutorial: https://github.com/Xilinx/Vitis-Tutorials/tree/2022.1/AI_Engine_Development/Feature_Tutorials/05-AI-engine-versal-integration
- the AI Engine Programming Environment guide
- Xilinx support forum

**Note** this folder exploits part of the options available in the original Xilinx tutorial.

## Configuration

The sample requires Vitis `2022.1`.

In order to run this sample on the `fpga0` server you need to properly setup the environment:

```bash
export PLATFORM_REPO_PATHS=/opt/Xilinx/Vitis/2022.1/base_platforms/
export XILNX_TOOLS_LOCATION=/opt/Xilinx/Vitis/2022.1
export XILINX_VERSAL=/opt/Xilinx/xilinx-versal-common-v2022.1

# Setup SDKTARGETSYSROOT and CXX (note that you may be required to unset LD_LIBRARY_PATH)                                                  
source $XILINX_VERSAL/ir/environment-setup-cortexa72-cortexa53-xilinx-linux
```

## Directory structure

There are two versions of the described program, one uses "windows" for data communication between PS and AIE and the other one uses "streams":  
- `manual_stream` uses streams
- `manual_window` uses windows  

The samples are organize as follows:
- the `aie` folder contains the AI-Engine source files (kernels and graph)
- the `pl_kernels` folder contains the PL kernels (data movers from DRAM to AIE and viceversa)
- the `sw` folder contains the host program
- the  `data` folder contains the input data and expected output

To run either of the versions `cd` into one of the examples and then use the following `make` commands.

## Simulate on the server

It is possible to run the AI Engine code directly on the server, using the `x86simulator`.
This can be used to quickly assess the functionality of the code, *without any guarantee that it will be functional on the Versal device*.


```bash
make aie
make sim
```

Once the simulation is completed, the output result can be found in the `x86simulator_output`.
For this particular example, the computed results are stored in  the `x86simulator_output/data/output.txt`, that can be then compared with
the expected results stored in `programmin_guide_example/data/golden.txt`.

```bash
diff <(head -32 x86simulator_output/data/output.txt) <(head -32 ../data/golden.txt)
```

For viewing other information with the vitis analyzer please follow the instructions at [Xilinx tutorial](https://github.com/Xilinx/Vitis-Tutorials/tree/2022.1/AI_Engine_Development/Feature_Tutorials/05-AI-engine-versal-integration#section-1-compile-ai-engine-code-using-the-ai-engine-compiler-for-x86simulator-viewing-compilation-results-in-vitis-analyzer)


## Building and runfor software emulation


Once that we are happy with the AI-Engine program we need to build the PL kernels, the host program and combine all together in a package
that will be then executed on the versal device.

For compiling the `mm2s` and `s2mm` HLS kernels run the following command:

```bash
make kernels
```

The we can link together the AI Engine program with the PL kernels ([Xilinx Tutorial](https://github.com/Xilinx/Vitis-Tutorials/tree/2022.1/AI_Engine_Development/Feature_Tutorials/05-AI-engine-versal-integration#2-use-v-to-link-ai-engine-hls-kernels-with-the-platform))

```bash
make xsa
```

We compile the host program with:

```bash
make host
```

Finally, we can build the packaged SD directory containing the linux image (that can be used to boot the device), the host program
and the `.xclbin` binary:
```bash
make package
```

At this point we can run the software emulation with

```bash
make run_emu
```

This will boot the Linux image as in the versal device, and the prompt is presented.
The program can be found under the mounted SD card:

```bash
cd /run/media/*1
./embedded_exec.sh
```

At the end, you should see the `TEST PASSED` printout. 

**Note:** to exit from software emulation, press `Ctrl+a` followed by `x` (then, likely, a `Ctrl+C` is needed)


### Running HW_EMU

Please refer to: [Xilinx Tutorial - Run the hardware emulation] (https://github.com/Xilinx/Vitis-Tutorials/tree/2022.1/AI_Engine_Development/Feature_Tutorials/05-AI-engine-versal-integration#section-6-run-the-hardware-emulation-and-view-run-summary-in-vitis-analyzer)

Result can be inspected with the Vitis GUI.

### Running in HW

For compiling the design in Hardware, the flow is pretty much the same, but we need to pass the `TARGET=hw` flag to the compilation stages.

**Note:** the flow will require few minutes to complete


```bash
make clean
make aie TARGET=hw
make kernels TARGET=hw
make xsa TARGET=hw
make host # no need to use Target here
make package TARGET=hw
```


## HW Execution
To run them in hw on the board you need to copy three files to the board:  
```manual_*/sw/host.exe```  
```manual_*/sw/xrt.ini```  
```manual_*/sw/a.xclbin```  
And then run the program with the command:  
```./host.exe a.xclbin```


