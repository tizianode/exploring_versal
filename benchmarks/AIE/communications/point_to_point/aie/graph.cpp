
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"

/*************************************************************************************\
* This programm is used for all the possible simulations and the hardware execution  *
* It is used to:                                                                     *
* - perform the timing of the benchmarks                                             *
* - initialize and start the AI-Engine graph for different amount of sizes           *
\*************************************************************************************/

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"

#define __TIMER__
#endif

#ifdef __TIMER__
#include <chrono>
#include <ctime>
class Timer
{
  std::chrono::high_resolution_clock::time_point mTimeStart;

public:
  Timer() { reset(); }
  long long stop()
  {
    std::chrono::high_resolution_clock::time_point timeEnd =
        std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                 mTimeStart)
        .count();
  }
  void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};

#endif

simpleGraph mygraph;
int main(int argc, char *argv[])
{
  // Setup standard values
  int numOfRuns = 1;                       // Number of times repeating the measurement
  unsigned long long actual_size;          // Used later as Variable
  int64 numOfSamplesPerIteration = 4096;   // Number of samples per Iteration (keep in mind, that one sample is of size 4 Bytes ==> dataToBeSent >= 4 * numOfSamplesPerIteration)
  unsigned long long dataToBeSent = 16384; // The total amount of data that should be sent; must be multiple of (numOfSamplesPerIteration * 4)
  adf::return_code ret;

  // Setup benchmark data file
  std::string csvName = "P2P-" + std::to_string(WINDOW_SIZE) + ".csv";
  std::ofstream ResultFile(csvName);
  ResultFile << "WindowSize,Data,Bandwidth\n";

// Get user input only if not in x86simulation or aiesimulation
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc < 5)
  {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << "<xclbin file> <no of iterations> <samples per iteration> <data to be sent>" << std::endl;
    std::cerr << "AND: data_to_be_sent >= 4 * samples_per_iteration must be true" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
  unsigned long long input = atoi(argv[2]);
  sscanf(argv[3], "%ld", &numOfSamplesPerIteration);
  sscanf(argv[4], "%llu", &dataToBeSent);
  numOfRuns = input;

  if (input == 1)
  {
    numOfSamplesPerIteration = 4096;
    dataToBeSent = 16384;
    std::cout << "Since number of iterations is 1 default values will be applied: " << std::endl;
    std::cout << "Samples per iteration: " << numOfSamplesPerIteration << std::endl;
    std::cout << "Total amount of data to be sent: " << dataToBeSent << std::endl;
  }
#else // if in simulation use small numbers of iterations
  numOfRuns = 1; //<--------------- set the number of iterations
#endif

  if ((dataToBeSent % (sizeof(int32) * numOfSamplesPerIteration)) != 0)
  {
    std::cout << "Data_to_be_sent needs to be a multiple of 4 * numOfSamplesPerIteration!" << std::endl;
    exit(-1);
  }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  // Create XRT device handle for ADF API
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif

  unsigned long long iterations;
  double throughput = 0;

  std::cout << "Number of Runs: " << numOfRuns << std::endl;

  for (int j = 0; j < numOfRuns; j++)
  {

    actual_size = dataToBeSent; // GB
    iterations = dataToBeSent / (numOfSamplesPerIteration * sizeof(int32));

    // compute the number of elements that must be transferred
    unsigned long long length = ceil(actual_size / sizeof(int32));
    std::cout << "Iteration " << j << ": Sending " << length << " elements (" << actual_size << " bytes). # of kernel iterations: " << iterations << "." << std::endl;

    mygraph.init();

    mygraph.update(mygraph.size_value_rcv, numOfSamplesPerIteration);
    mygraph.update(mygraph.size_value_snd, numOfSamplesPerIteration);

#ifdef __TIMER__
    // timing
    Timer timer;
#endif
    ret = mygraph.run(iterations);
    if (ret != adf::ok)
    {
      printf("Host: Run failed\n");
      return ret;
    }

    mygraph.wait(); // wait for iteration(s) to finish
#ifdef __TIMER__
    long long timer_stop = timer.stop();
    // printf("Execution time (usecs): %lld\n", timer_stop);
    throughput = ((actual_size / timer_stop) * 1e6) / 1024 / 1024;
    // printf("Throughput (MB/s): %f\n", throughput);
#endif

    ResultFile << WINDOW_SIZE << "," << (actual_size / 1024 > 1024 ? (actual_size / 1024 / 1024 > 1024 ? std::to_string(actual_size / 1024 / 1024 / 1024) + " GB" : std::to_string(actual_size / 1024 / 1024) + " MB") : std::to_string(actual_size / 1024) + " KB") << "," << throughput << std::endl;
  }

  ResultFile.close();

  ret = mygraph.end();
  if (ret != adf::ok)
  {
    printf("End failed\n");
    return ret;
  }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}
