#ifndef FUNCTION_STREAM_H
#define FUNCTION_STREAM_H

#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"

#define NUM_SAMPLES 2048 // Number of samples that are processed per kernel Iteration
                         // While we are specifing this number of samples at compile time, this could also be implemented using a runtime parameter.
                         // Then it would be possible to change the amount during runtime.
/*
    Kernel that sends NUM_SAMPLES ones using GMIO or 32 bit width PLIO streams
*/
void kernel_stream(output_stream<int> *out);

#endif
