#include <adf.h>
#include "kernels/window.h"
#include "kernels/stream.h"
#include "kernels/window_64.h"
#include "kernels/stream_64.h"
#include "kernels/window_128.h"
#include "kernels/stream_128.h"

using namespace adf;

// NOTES:
// Since there are enough GMIO and PLIO ports available we can initialize various different graphs
// and then choose in the host program what we want to benchmark

// Window based GMIO transfer
class windowGraphGMIO : public graph
{
private:
    kernel kern_window_gmio;

public:
    input_gmio in_window_gmio;
    output_gmio out_window_gmio;

    windowGraphGMIO()
    {

        printf("Init\n");

        // create gmio
        in_window_gmio = input_gmio::create("in_window_gmio", 64, 1000);
        out_window_gmio = output_gmio::create("out_window_gmio", 64, 1000);
        printf("GMIO created\n");

        kern_window_gmio = kernel::create(kernel_window);

        connect<window<NUM_SAMPLES * sizeof(int)>> net0(in_window_gmio.out[0], kern_window_gmio.in[0]);
        connect<window<2 * sizeof(int)>> net1(kern_window_gmio.out[0], out_window_gmio.in[0]);

        source(kern_window_gmio) = "kernels/window.cpp";

        runtime<ratio>(kern_window_gmio) = 0.1;
    }
};

// Stream based GMIO transfer
class streamGraphGMIO : public graph
{
private:
    kernel kern_stream_gmio;

public:
    input_gmio in_stream_gmio;
    output_gmio out_stream_gmio;

    streamGraphGMIO()
    {

        printf("Init\n");

        // create gmio
        in_stream_gmio = input_gmio::create("in_stream_gmio", 64, 1000);
        out_stream_gmio = output_gmio::create("out_stream_gmio", 64, 1000);
        printf("GMIO created\n");

        kern_stream_gmio = kernel::create(kernel_stream);

        connect<stream> net0(in_stream_gmio.out[0], kern_stream_gmio.in[0]);
        connect<stream> net1(kern_stream_gmio.out[0], out_stream_gmio.in[0]);

        source(kern_stream_gmio) = "kernels/stream.cpp";

        runtime<ratio>(kern_stream_gmio) = 0.1;
    }
};

// Window based PLIO transfer
class windowGraphPLIO : public graph
{
private:
    kernel kern_window_plio;

public:
    input_plio in_window_plio;
    output_plio out_window_plio;
    windowGraphPLIO()
    {
        printf("Init\n");

        in_window_plio = input_plio::create("in_window_plio", plio_32_bits, "data/in.txt");
        out_window_plio = output_plio::create("out_window_plio", plio_32_bits, "data/outputwindow.txt");
        printf("PLIO created\n");

        kern_window_plio = kernel::create(kernel_window);

        connect<window<NUM_SAMPLES * sizeof(int)>> net0(in_window_plio.out[0], kern_window_plio.in[0]);
        connect<window<2 * sizeof(int)>> net1(kern_window_plio.out[0], out_window_plio.in[0]);
        source(kern_window_plio) = "kernels/window.cpp";

        runtime<ratio>(kern_window_plio) = 0.1;
    }
};

// Stream based GMIO transfer
class streamGraphPLIO : public graph
{
private:
    kernel kern_stream_plio;

public:
    input_plio in_stream_plio;
    output_plio out_stream_plio;

    streamGraphPLIO()
    {

        printf("Init\n");

        // create gmio
        in_stream_plio = input_plio::create("in_stream_plio", plio_32_bits, "data/in.txt");
        out_stream_plio = output_plio::create("out_stream_plio", plio_32_bits, "data/outputstream.txt");
        printf("PLIO created\n");

        kern_stream_plio = kernel::create(kernel_stream);

        connect<stream> net0(in_stream_plio.out[0], kern_stream_plio.in[0]);
        connect<stream> net1(kern_stream_plio.out[0], out_stream_plio.in[0]);

        source(kern_stream_plio) = "kernels/stream.cpp";

        runtime<ratio>(kern_stream_plio) = 0.1;
    }
};

// PLIO with 64 bit width and windows
class windowGraphPLIO64 : public graph
{
private:
    kernel kern_window_plio_64;

public:
    input_plio in_window_plio_64;
    output_plio out_window_plio_64;
    windowGraphPLIO64()
    {
        printf("Init\n");

        in_window_plio_64 = input_plio::create("in_window_plio_64", plio_64_bits, "data/in64.txt");
        out_window_plio_64 = output_plio::create("out_window_plio_64", plio_64_bits, "data/outputwindow64.txt");
        printf("PLIO created\n");

        kern_window_plio_64 = kernel::create(kernel_window_64);

        connect<window<NUM_SAMPLES * sizeof(int)>> net0(in_window_plio_64.out[0], kern_window_plio_64.in[0]);
        connect<window<2 * sizeof(int)>> net1(kern_window_plio_64.out[0], out_window_plio_64.in[0]);
        source(kern_window_plio_64) = "kernels/window_64.cpp";

        runtime<ratio>(kern_window_plio_64) = 0.1;
    }
};

// PLIO with 64 bit width and streams
class streamGraphPLIO64 : public graph
{
private:
    kernel kern_stream_plio_64;

public:
    input_plio in_stream_plio_64;
    output_plio out_stream_plio_64;

    streamGraphPLIO64()
    {

        printf("Init\n");

        // create gmio
        in_stream_plio_64 = input_plio::create("in_stream_plio_64", plio_64_bits, "data/in64.txt");
        out_stream_plio_64 = output_plio::create("out_stream_plio_64", plio_64_bits, "data/outputstream64.txt");
        printf("PLIO created\n");

        kern_stream_plio_64 = kernel::create(kernel_stream_64);

        connect<stream> net0(in_stream_plio_64.out[0], kern_stream_plio_64.in[0]);
        connect<stream> net1(kern_stream_plio_64.out[0], out_stream_plio_64.in[0]);

        source(kern_stream_plio_64) = "kernels/stream_64.cpp";

        runtime<ratio>(kern_stream_plio_64) = 0.1;
    }
};

// PLIO with 128 bit width and windows
class windowGraphPLIO128 : public graph
{
private:
    kernel kern_window_plio_128;

public:
    input_plio in_window_plio_128;
    output_plio out_window_plio_128;
    windowGraphPLIO128()
    {
        printf("Init\n");

        in_window_plio_128 = input_plio::create("in_window_plio_128", plio_128_bits, "data/in128.txt");
        out_window_plio_128 = output_plio::create("out_window_plio_128", plio_128_bits, "data/outputwindow128.txt");
        printf("PLIO created\n");

        kern_window_plio_128 = kernel::create(kernel_window_128);

        connect<window<NUM_SAMPLES * sizeof(int)>> net0(in_window_plio_128.out[0], kern_window_plio_128.in[0]);
        connect<window<4 * sizeof(int)>> net1(kern_window_plio_128.out[0], out_window_plio_128.in[0]);
        source(kern_window_plio_128) = "kernels/window_128.cpp";

        runtime<ratio>(kern_window_plio_128) = 0.1;
    }
};

// PLIO with 128 bit width and streams
class streamGraphPLIO128 : public graph
{
private:
    kernel kern_stream_plio_128;

public:
    input_plio in_stream_plio_128;
    output_plio out_stream_plio_128;

    streamGraphPLIO128()
    {

        printf("Init\n");

        // create gmio
        in_stream_plio_128 = input_plio::create("in_stream_plio_128", plio_128_bits, "data/in128.txt");
        out_stream_plio_128 = output_plio::create("out_stream_plio_128", plio_128_bits, "data/outputstream128.txt");
        printf("PLIO created\n");

        kern_stream_plio_128 = kernel::create(kernel_stream_128);

        connect<stream> net0(in_stream_plio_128.out[0], kern_stream_plio_128.in[0]);
        connect<stream> net1(kern_stream_plio_128.out[0], out_stream_plio_128.in[0]);

        source(kern_stream_plio_128) = "kernels/stream_128.cpp";

        runtime<ratio>(kern_stream_plio_128) = 0.1;
    }
};
