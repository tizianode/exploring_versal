#ifndef FUNCTION_STREAM_64_H
#define FUNCTION_STREAM_64_H

#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"

#define NUM_SAMPLES 2048 // Number of samples that are processed per kernel Iteration
                         // While we are specifing this number of samples at compile time, this could also be implemented using a runtime parameter.
                         // Then it would be possible to change the amount during runtime.
/*
    Kernel that sums all NUM_SAMPLES Numbers up and sends it back
*/
void kernel_stream_64(input_stream<int> *in_x, output_stream<int> *out);

#endif
