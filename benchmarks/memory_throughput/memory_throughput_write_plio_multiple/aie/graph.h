#ifndef __GRAPH_H_
#define __GRAPH_H_
#include <adf.h>
#include "kernels/window.h"

using namespace adf;

// Defines the number of used AIES and therefore PLIO ports
#define nAIE 4

// Graph that has nAIE AI-Engines with each having one PLIO output port and a window for communication
class windowGraphPLIO : public graph
{
private:
    kernel kern_window_plio[nAIE];

public:
    output_plio out_window_plio[nAIE];
    windowGraphPLIO()
    {
        printf("Init\n");

        // create plio ports
        for (int i = 0; i < nAIE; i++)
        {
            printf("Creating PLIO %d:\n", i);
            out_window_plio[i] = output_plio::create("out_window_plio" + std::to_string(i), plio_128_bits, "data/outputwindow" + std::to_string(i) + ".txt");
            kern_window_plio[i] = kernel::create(kernel_window);
            connect<window<NUM_SAMPLES * sizeof(int)>>(kern_window_plio[i].out[0], out_window_plio[i].in[0]);
            source(kern_window_plio[i]) = "kernels/window.cpp";
            runtime<ratio>(kern_window_plio[i]) = 1;
        }
    }
};

#endif