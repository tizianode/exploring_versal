#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H

#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"

#define NUM_SAMPLES 2048 // length of vector x and y
/*
	Kernel to sum two vectors x and y
*/
void vecsum_kernel(input_window<float> *in_x, input_window<float> *in_y, output_window<float> *out);

#endif
