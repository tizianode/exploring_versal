/***************************************************************************\
* This programm is used for x86simulation and aiesimulation.                *
* It can be used to check, that:                                            *
* - the graph is set up correctly                                           *
* - it is computing the correct output                                      *
\***************************************************************************/

#include "graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>

// To use GMIO in Hardware flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"
// Used for basic timing
#include <chrono>
#include <ctime>
class Timer
{
    std::chrono::high_resolution_clock::time_point mTimeStart;

public:
    Timer() { reset(); }
    long long stop()
    {
        std::chrono::high_resolution_clock::time_point timeEnd =
            std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                     mTimeStart)
            .count();
    }
    void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};
#endif

using namespace adf;

// Initialisation of the graph
windowGraphGMIO mygraph;

const int numOfTests = 3;
const int numSamples[15] = {
    4096,
    8192,
    16384,
    32768,
    327680,
    524288,
    1048576,
    2097152,
    5242880,
    10485760,
    20971520,
    41943040,
    68157440,
    104857600};

int benchmark_gmio()
{
    adf::return_code ret;

    // Starting graph
    mygraph.init();
    for (int i = 0; i < numOfTests; i++)
    {
        int n = numSamples[i];
        if (n % NUM_SAMPLES != 0)
        {
            std::cout << n << " is not multiple of" << NUM_SAMPLES << "! " << n % NUM_SAMPLES << std::endl;
            return -1;
        }

        if (nAIE * 4 >= 1073741824)
            break;

        std::cout << "Running Tests for " << n << " Samples" << std::endl;
        int NUM_SAMPLES_TOTAL = n;

        int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;

        int BLOCK_SIZE_in_Bytes = NUM_SAMPLES_TOTAL * sizeof(int);
        int BLOCK_SIZE_out_Bytes = ITERATIONS * 2 * sizeof(int);

        // GMIO Malloc all needed arrays
        int *inArray[nAIE];
        int *outArray[nAIE];
        int *outGolden = new int[ITERATIONS * 2];

        for (int k = 0; k < nAIE; k++)
        {
            inArray[k] = (int *)GMIO::malloc(BLOCK_SIZE_in_Bytes);
            outArray[k] = (int *)GMIO::malloc(BLOCK_SIZE_out_Bytes);
        }

        // Init data arrays
        for (int k = 0; k < nAIE; k++)
            for (int i = 0; i < NUM_SAMPLES_TOTAL; i++)
            {
                inArray[k][i] = 1;
            }

        // Init reference result arrays
        for (int i = 0; i < ITERATIONS * 2; i += 2)
        {
            outGolden[i] = NUM_SAMPLES;
            outGolden[i + 1] = 0;
        }

        // run the graph
        ret = mygraph.run(ITERATIONS);

        if (ret != adf::ok)
        {
            printf("Run failed\n");
            return -1;
        }

        // connect the gmio of the graph with the arrays
        for (int k = 0; k < nAIE; k++)
            mygraph.in_window_gmio[k].gm2aie_nb(inArray[k], BLOCK_SIZE_in_Bytes);

        // get the ouput from the aie
        for (int k = 0; k < nAIE; k++)
        {
            mygraph.out_window_gmio[k].aie2gm_nb(outArray[k], BLOCK_SIZE_out_Bytes);
            mygraph.out_window_gmio[k].wait();
        }
        // wait for the graph to finish execution
        mygraph.wait();

        int errorCount = 0;
        // Verification of the results
        for (int k = 0; k < nAIE; k++)
            for (int j = 0; j < ITERATIONS * 2; j++)
            {
                if (outArray[k][j] != outGolden[j])
                {
                    std::cout << "ERROR:dout[" << j << "]=" << outArray[k][j] << ",gold=" << outGolden[j] << std::endl;
                    errorCount++;
                }
            }
        if (errorCount)
            printf("Test failed with %d errors\n", errorCount);
        else
            printf("TEST PASSED\n");

        // finishing up
        for (int k = 0; k < nAIE; k++)
        {
            GMIO::free(inArray[k]);
            GMIO::free(outArray[k]);
        }
        delete[] outGolden;
    }

    ret = mygraph.end();

    if (ret != adf::ok)
    {
        printf("End failed\n");
        return -1;
    }
    return 0;
}

int main(int argc, char **argv)
{
    std::cout << "----- Memory throughput benchmark -----" << std::endl;
    std::cout << "Benchmarking GMIO window: " << std::endl;
    benchmark_gmio();
    return 0;
}
