# PLATFORM := xilinx_vck190_base_202210_1
PLATFORM := xilinx_vck190_base_dfx_202210_1

# Use this one for hard and software emulation (faster compilation time)
BASE_PLATFORM := ${PLATFORM_REPO_PATHS}/xilinx_vck190_base_202210_1/xilinx_vck190_base_202210_1.xpfm
# Use this one for real hardware running (long compilation time)
BASE_PLATFORM_DFX := ${PLATFORM_REPO_PATHS}/xilinx_vck190_base_dfx_202210_1/xilinx_vck190_base_dfx_202210_1.xpfm 

SDKTARGETSYSROOT ?= ${SYSROOT}
ROOTFS := ${XILINX_VERSAL}/rootfs.ext4
IMAGE := ${XILINX_VERSAL}/Image

# Makefile input options
TARGET   := hw

# to store binaries
TIMESTAMP = $(shell date +'%F-%T')

# File names and locations
GRAPH := aie/graph.cpp
GRAPH_O  := libadf.a
# KERNEL := s2mm.cpp mm2s.cpp
KERNEL := s2mm.cpp s2mm_64.cpp s2mm_128.cpp
# KERNEL_XO := s2mm.xo mm2s.xo
KERNEL_XO := s2mm.xo s2mm_64.xo s2mm_128.xo
# CONFIG_FILE := link.cfg

HOST_EXE = host.exe
XSA	 = vck190_aie_base_graph_${TARGET}.xsa

###########################################################################


VCC      = v++
AIECC := aiecompiler
AIESIM := aiesimulator
X86SIM := x86simulator
VPP_FLAGS=--save-temps --verbose
# VPP_LINK_FLAGS := -l -t $(TARGET) --platform $(BASE_PLATFORM_DFX) $(KERNEL_XO) $(GRAPH_O)  --save-temps -g --config $(CONFIG_FILE) -o $(XSA)
VPP_LINK_FLAGS := -l -t $(TARGET) --platform $(BASE_PLATFORM_DFX)  $(GRAPH_O)  --save-temps -g  -o $(XSA)
VPP_XO_FLAGS := -c --platform $(BASE_PLATFORM_DFX) -t $(TARGET) --save-temps -g
LDCLFLAGS=

HW_EMU_CMD := ./launch_hw_emu.sh -aie-sim-options ../aiesimulator_output/aiesim_options.txt -add-env AIE_COMPILER_WORKDIR=../Work 
SW_EMU_CMD := ./launch_sw_emu.sh 

AIE_INCLUDE_FLAGS := -include="$(XILINX_VITIS)/aietools/include" -include="./aie" -include="./data" -include="./aie/kernels" -include="./"
AIE_FLAGS := $(AIE_INCLUDE_FLAGS) -v -workdir=./Work

ifeq ($(TARGET),sw_emu)
	AIE_FLAGS += --target=x86sim
else
	AIE_FLAGS += --target=hw
endif 

###########################################################################

all: clean_all aie_compile sim xsa host package


###################
# AIE Simulation
###################
.ONESHELL:
.PHONY: clean aie_compile sim xsa host package run_emu

#AIE or X86 compilation
aie_compile: clean_all

ifeq ($(TARGET),sw_emu)	# X86 compilation
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO:Running aiecompiler for x86simulator..."
	@rm -rf Work libadf.a
	@mkdir -p Work
	$(AIECC) $(AIE_FLAGS) $(GRAPH)
	@echo "COMPLETE: libadf.a created for x86simulator and sw_emu."
else					# AIE compilation
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO:Running aiecompiler for hw..."
	@rm -rf Work libadf.a
	@mkdir -p Work
	$(AIECC) $(AIE_FLAGS) $(GRAPH)
	@echo "COMPLETE: libadf.a created for hw."
endif

#AIE or X86 Simulation
sim: 
     
ifeq ($(TARGET),sw_emu) # X86 Simulation
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO: Running x86simulator..."
	$(X86SIM) --pkg-dir=./Work
	# @echo "INFO: Comparing Results..."	
	# @echo "PLIO Window 32bit:"
	# diff x86simulator_output/data/outputwindow.txt data/golden.txt
	# @echo "PLIO Stream 32bit:"
	# diff x86simulator_output/data/outputstream.txt data/golden.txt
	# @echo "PLIO Window 64bit:"
	# diff x86simulator_output/data/outputwindow_64.txt data/golden_64.txt
	# @echo "PLIO Stream 64bit:"
	# diff x86simulator_output/data/outputstream_64.txt data/golden_64.txt
	# @echo "PLIO Window 128bit:"
	# diff x86simulator_output/data/outputwindow_128.txt data/golden_128.txt
	# @echo "PLIO Stream 128bit:"
	# diff x86simulator_output/data/outputstream_128.txt data/golden_128.txt
#@diff -w data/golden.txt x86simulator_output/data/output.txt
	@echo "INFO: OK!"
else 					# AIE Simulation
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO: Running aiesimulator..."
	$(AIESIM) --profile --dump-vcd=tutorial --pkg-dir=./Work --output-time-stamp=no
	# @echo "Output comparison PLIO:"
	# @echo "Window 32bit:"
	# diff aiesimulator_output/data/outputwindow.txt data/golden.txt
	# @echo "Stream 32bit:"
	# diff aiesimulator_output/data/outputstream.txt data/golden.txt
	# @echo "Window 64bit:"
	# diff aiesimulator_output/data/outputwindow_64.txt data/golden_64.txt
	# @echo "Stream 64bit:"
	# diff aiesimulator_output/data/outputstream_64.txt data/golden_64.txt
	# @echo "Window 128bit:"
	# diff aiesimulator_output/data/outputwindow_128.txt data/golden_128.txt
	# @echo "Stream 128bit:"
	# diff aiesimulator_output/data/outputstream_128.txt data/golden_128.txt
endif 
# diff aiesimulator_output/data/outputwindow.txt data/golden.txt

# diff aiesimulator_output/data/outputstream.txt data/golden.txt
	
###################
#   Hardwareflow   #
###################

######################################################
# This step compiles the HLS C kernels and creates an ADF Graph
# the %.xo is used as the output and creates from the %.cpp files
# The graph is generated by having the -target=hw
kernels: 
	$(VCC) $(VPP_XO_FLAGS) -k s2mm pl_kernels/s2mm.cpp -o s2mm.xo
	$(VCC) $(VPP_XO_FLAGS) -k s2mm_64 pl_kernels/s2mm_64.cpp -o s2mm_64.xo
	$(VCC) $(VPP_XO_FLAGS) -k s2mm_128 pl_kernels/s2mm_128.cpp -o s2mm_128.xo
# $(VCC) $(VPP_XO_FLAGS) -k mm2s pl_kernels/mm2s.cpp -o mm2s.xo
	@echo "COMPLETE: Kernels Created."	


########################################################
# Once the graph is generated, you can build
# the hardware part of the design. This creates an xsa
# that will be used to run the design on the platform.
xsa: ${XSA}

# ${XSA}: aie_compile kernels ${VPP_SPEC} 
${XSA}:
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO: Running xsa generation..."
	$(VCC) $(VPP_LINK_FLAGS) || (echo "task: [xsa] failed error code: $$?"; exit 1)
	@echo "COMPLETE: .xsa created."
########################################################

############################################################################################################################
# For sw emulation, hw emulation and hardware, compile the PS code and generate the host.exe. This is needed for creating the sd_card.
host: ${HOST_EXE}
${HOST_EXE}: ${GRAPH} ./Work/ps/c_rts/aie_control_xrt.cpp
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO: Running host.exe generation..."
	$(MAKE) -C sw/
	@echo "COMPLETE: host.exe created."

############################################################################################################################

##################################################################################################
# Depending on the TARGET, it'll either generate the PDI for sw_emu,hw_emu or hw.
# package: aie sim kernels ${XSA} ${HOST_EXE} 
package: 
ifeq ($(TARGET),hw)
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO: Running package for hw..."
	cd ./sw
	v++ -p -t hw \
        -f ${BASE_PLATFORM_DFX} \
        --package.defer_aie_run \
        -o a.xclbin \
        ../${XSA} ../${GRAPH_O}
	v++ -p -t hw \
		-f ${BASE_PLATFORM_DFX} \
		--package.rootfs ${ROOTFS} \
		--package.image_format=ext4 \
		--package.kernel_image ${IMAGE} \
		--package.boot_mode=sd \
		--package.sd_file xrt.ini \
		--package.sd_file ${HOST_EXE} \
		--package.sd_file a.xclbin
	cd ..
	make copy
	@echo "COMPLETE: hardware package created."
endif 
ifeq ($(TARGET),sw_emu)
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO: Running packaging for sw_emu..."
	cd ./sw
	${VCC} -p -t ${TARGET} -f ${BASE_PLATFORM} \
		--package.rootfs ${ROOTFS} \
		--package.kernel_image ${IMAGE} \
		--package.boot_mode=sd \
		--package.image_format=ext4 \
		--package.defer_aie_run \
		--package.sd_file embedded_exec_sw.sh \
		--package.sd_file ${HOST_EXE} \
		../${XSA} ../${GRAPH_O}
	@echo "COMPLETE: emulation package created."
endif
ifeq ($(TARGET),hw_emu)
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO: Running packaging..."
	cd ./sw
	${VCC} -p -t ${TARGET} -f ${BASE_PLATFORM} \
		--package.rootfs ${ROOTFS} \
		--package.kernel_image ${IMAGE} \
		--package.boot_mode=sd \
		--package.image_format=ext4 \
		--package.defer_aie_run \
		--package.sd_file embedded_exec.sh \
		--package.sd_file ${HOST_EXE} \
		../${XSA} ../${GRAPH_O}
	@echo "COMPLETE: emulation package created."
endif

###########################################################################

# Copy sourcecode and binaries to an archive folder
copy:
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO: Archiving files to binaries/${TARGET}/$(TIMESTAMP)" 
ifeq ($(TARGET),hw)
	cd sw
	mkdir -p ../binaries/hw/$(TIMESTAMP)/sw
	mkdir -p ../binaries/hw/$(TIMESTAMP)/benchmarks
	cp a.xclbin ../binaries/hw/$(TIMESTAMP)/sw
	cp ${HOST_EXE} ../binaries/hw/$(TIMESTAMP)/sw
	cp xrt.ini ../binaries/hw/$(TIMESTAMP)/sw
	cp host.cpp ../binaries/hw/$(TIMESTAMP)/sw
	cp aie_control_xrt.cpp ../binaries/hw/$(TIMESTAMP)/sw
	cp ../${XSA} ../binaries/hw/$(TIMESTAMP)
	cp ../${GRAPH_O} ../binaries/hw/$(TIMESTAMP)
	cp -r ../aie ../binaries/hw/$(TIMESTAMP)
	cp ../Makefile ../binaries/hw/$(TIMESTAMP)
	cp ../scp.sh ../binaries/hw/$(TIMESTAMP)
	@echo "COMPLETE: copied all files"
endif


###########################################################################

#Build the design and then run sw/hw emulation 
run: all run_emu

###########################################################################
run_emu: 
# If the target is for SW_EMU, launch the emulator
ifeq (${TARGET},sw_emu)
	cd ./sw
	$(SW_EMU_CMD)
else
# If the target is for HW_EMU, launch the emulator
ifeq (${TARGET},hw_emu)
	cd ./sw
	$(HW_EMU_CMD)
else
	@echo "Hardware build, no emulation executed."
endif
endif

###########################################################################
clean_all: clean
	rm -rf _x v++* *.o *.compile_summary* $(GRAPH_O) *.xpe xnwOut *.xclbin* *.log *.xsa Work *.db *.csv *.jou .Xil .run .AIE_SIM_CMD_LINE_OPTIONS ISS_RPC_SERVER_PORT pl_sample_counts *.vcd *.link_summary*
	rm -rf sw/qemu* sw/sim vitis_analyzer* sw/pl_script.sh sw/root@versal2_ sw/x86simulator_output .ipcache sw/graph.cpp

clean:
	rm -rf sw/*.log sw/*.xclbin sw/cfg/ sw/launch_sw_emu.sh sw/launch_hw_emu.sh sw/qemu_dts_files sw/emu_qemu_scripts sw/*.exe sw/_x/ sw/*summary sw/*.o sw/*.elf sw/*.xpe sw/xnwOut sw/Work sw/*.csv sw/*.db sw/*.bin sw/*.BIN sw/*.bif sw/launch_hw_emulator.sh sw/*.txt sw/emulation sw/.Xil ./x86simulator_output
	rm -rf sw/sd_card sw/sd_card.img sw/*.o ./*.exe sw/qeumu* x86simulator_output/ aiesimulator_output/
	rm -rf *.log

clean_for_x86:
	rm -rf sw/sd_card sw/sd_card.img sw/*.o sw/*.exe sw/qemu* sw/launch_sw_* 
	rm -rf sw/*.log sw/*.xclbin sw/cfg/ sw/launch_hw_emu.sh sw/qemu_dts_files sw/emu_qemu_scripts sw/*.exe sw/_x/ sw/*summary sw/*.o sw/*.elf sw/*.xpe sw/xnwOut sw/Work sw/*.csv sw/*.db sw/*.bin sw/*.BIN sw/*.bif sw/launch_hw_emulator.sh sw/*.txt sw/emulation sw/.Xil

