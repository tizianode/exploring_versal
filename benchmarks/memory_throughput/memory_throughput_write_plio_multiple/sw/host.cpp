/*******************************************************************************\
* This host program can be used to benchmark the throughput between multiple    *
* AIEs and the DDR Memory of the board (i.e. memory read). "nAIE" is defined     *
* and can be changed in graph.h.                                                *
* This was here only implemented for PLIO based data transfer in combination    *
* with windows.                                                                 *
* The variables "numOfRuns", "numOfTests" and "numSamples" are used to define:   *
* "numOfRuns": How often should the benchmark be run                            *
* "numOfTests": until what index of the numSamples array should be tested       *
* "numSamples": an array containing different number of samples that will be    *
*               sent/read                                                       *
* Note that the maximum allocatable memory is bound by the CMA of the OS, in    *
* this case 1 GB (see line 136)                                                 *
* The throughput results of the benchmark are then written to a csv file which   *
* name is defined in line 115.                                                   *
\*******************************************************************************/

#include "../aie/graph.h"
#include <adf.h>
#include <fstream>
#include <unistd.h>
#include <iostream>
#include <string>

// To use GMIO in Hardware flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
// This is used for the PL Kernels
#include "xrt.h"

#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"

// Used for basic timing
#include <chrono>
#include <ctime>
class Timer
{
    std::chrono::high_resolution_clock::time_point mTimeStart;

public:
    Timer() { reset(); }
    long long stop()
    {
        std::chrono::high_resolution_clock::time_point timeEnd =
            std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                     mTimeStart)
            .count();
    }
    void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};
#endif

using namespace adf;

// initialize graph
windowGraphPLIO mygraph;

// Set the number of Iterations and number of Tests to be done
const int numOfRuns = 1;
const int numOfTests = 5;
const int numSamples[5] = {
    4194304,  // 16 MB
    8388608,  // 32 MB
    16777216, // 64 MB
    33554432, // 128 MB
    67108864, // 256 MB
};

static std::vector<char>
load_xclbin(xrtDeviceHandle device, const std::string &fnm)
{
    if (fnm.empty())
        throw std::runtime_error("No xclbin specified");

    // load bit stream
    std::ifstream stream(fnm);
    stream.seekg(0, stream.end);
    size_t size = stream.tellg();
    stream.seekg(0, stream.beg);

    std::vector<char> header(size);
    stream.read(header.data(), size);

    auto top = reinterpret_cast<const axlf *>(header.data());
    if (xrtDeviceLoadXclbin(device, top))
        throw std::runtime_error("Xclbin loading failed");

    return header;
}

int main(int argc, char **argv)
{
    std::cout << "----- Benchmarks -----" << std::endl;

// To use GMIO in the Hardware  flow
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
    // Create XRT device handle for ADF API
    char *xclbinFilename = argv[1];
    auto dhdl = xrtDeviceOpen(0); // device index=0
    auto xclbin = load_xclbin(dhdl, xclbinFilename);
    auto top = reinterpret_cast<const axlf *>(xclbin.data());

    adf::registerXRT(dhdl, top->m_header.uuid);
#endif

    // std::cout << "Benchmarking PLIO Window 32bit: " << std::endl;
    // std::cout << "Benchmarking PLIO Window 64bit: " << std::endl;
    std::cout << "Benchmarking PLIO Window 128bit: " << std::endl;
    // change width accordingly (32, 64, or 128 bit)
    int width = 128;
    std::string typ = "window";

    // Automatic creation of a csv file for analysis
    std::string csvName = "PLIO-window-multiple-out-" << width << "bit-nAIE" << std::to_string(nAIE) << ".csv";
    std::ofstream ResultFile(csvName);
    ResultFile << "DataSent,Throughput\n";
    adf::return_code ret;

    // Starting graph
    mygraph.init();
    // Number of iterations to be executed
    for (int j = 0; j < numOfRuns; j++)
    {
        auto throughputs = new float[numOfTests];
        // until what size of samples
        for (int i = 0; i < numOfTests; i++)
        {
            int n = numSamples[i];
            if (n % NUM_SAMPLES != 0)
            {
                std::cout << n << " is not multiple of " << NUM_SAMPLES << "! " << n % NUM_SAMPLES << std::endl;
                return -1;
            }
            // If the needed memory is bigger than the CMA, break
            if (n * nAIE * 4 >= 1073741824)
                break;

            std::cout << "Iteration " << j << ":Running Tests for " << n << " Samples with " << nAIE << "AI-Engines" << std::endl;

            int NUM_SAMPLES_TOTAL = n;
            int ITERATIONS = NUM_SAMPLES_TOTAL / NUM_SAMPLES;
            int sizeOut = NUM_SAMPLES_TOTAL;

            //////////////////////////////////////////
            // input memory
            // Allocating the input size of sizeIn to MM2S
            // This is using low-level XRT call xclAllocBO to allocate the memory
            //////////////////////////////////////////

            int *outGolden = new int[sizeOut];

            // Init data arrays
            for (int i = 0; i < NUM_SAMPLES_TOTAL; i++)
            {
                outGolden[i] = 1;
            }

            //////////////////////////////////////////
            // output memory
            // Allocating the output size of sizeOut to S2MM
            // This is using low-level XRT call xclAllocBO to allocate the memory
            //////////////////////////////////////////

            xrtBufferHandle out_bohdl[nAIE];
            int *out_bomapped[nAIE];
            for (int k = 0; k < nAIE; k++)
            {
                out_bohdl[k] = xrtBOAlloc(dhdl, sizeOut * sizeof(int), 0, 0);
                out_bomapped[k] = reinterpret_cast<int *>(xrtBOMap(out_bohdl[k]));
                memset(out_bomapped[k], 0, sizeOut * sizeof(int));
            }

            xrtKernelHandle s2mm_khdl[nAIE];
            for (int k = 0; k < nAIE; k++)
            {
                // ATTENTION: this needs to be changed for different widths (also the link.cfg needs to be changed accordingly )
                std::string nameS = "s2mm_128:{s2mmw" + std::to_string(k);
                nameS += +"}";
                char name[20];
                std::strcpy(name, nameS.c_str());
                s2mm_khdl[k] = xrtPLKernelOpen(dhdl, top->m_header.uuid, name);
            }

            // run the graph
            ret = mygraph.run(ITERATIONS);
            if (ret != adf::ok)
            {
                printf("Run failed\n");
                return -1;
            }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
            const int WINDOW_SIZE_in_bytes = NUM_SAMPLES * sizeof(int); // Number of samples per iteration * sizeof(int)
            // start the timer
            Timer timer;
#endif
            // Timed part begin

            // start the memory transactions for each AIE
            xrtRunHandle s2mm_rhdl[nAIE];
            for (int k = 0; k < nAIE; k++)
            {
                s2mm_rhdl[k] = xrtKernelRun(s2mm_khdl[k], out_bohdl[k], nullptr, sizeOut / (width / 32));
            }

            // waiting for the graph to finish execution
            mygraph.wait();

// Timed part end
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
            // wait for the memory transactions to finish execution
            for (int k = 0; k < nAIE; k++)
            {
                xrtRunWait(s2mm_rhdl[k]);
            }

            // calculate throughput
            double timer_stop = timer.stop();
            double throughputTimer = ((double)WINDOW_SIZE_in_bytes * ITERATIONS * nAIE) / 1024 / 1024 / timer_stop * 1000000; // Mbytes per second
                                                                                                                              // Read performance counter value immediately
            // Assuming that overhead can be negligible if iteration is large enough
            throughputs[i] = throughputTimer;
#endif

            // close all the kernels
            for (int k = 0; k < nAIE; k++)
            {
                xrtRunWait(s2mm_rhdl[k]);
                xrtRunClose(s2mm_rhdl[k]);
                xrtKernelClose(s2mm_khdl[k]);
            }

#if defined(__SYNCBO_ENABLE__)
            for (int k = 0; k < nAIE; k++)
                xrtBOSync(out_bohdl[k], XCL_BO_SYNC_BO_FROM_DEVICE, sizeOut * sizeof(int), 0);
#endif
            int errorCount = 0;
            // Verification of the result
            for (int k = 0; k < nAIE; k++)
                for (int j = 0; j < sizeOut; j++)
                {
                    if (out_bomapped[k][j] != outGolden[j])
                    {
                        std::cout << "ERROR:dout[" << j << "]=" << out_bomapped[k][j] << ",gold=" << outGolden[j] << std::endl;
                        errorCount++;
                    }
                }

            if (errorCount)
                printf("Test failed with %d errors\n", errorCount);

            // finishing up
            for (int k = 0; k < nAIE; k++)
                xrtBOFree(out_bohdl[k]);
            delete[] outGolden;
        }

        // Save results
        for (int i = 0; i < numOfTests; i++)
        {
            ResultFile << (numSamples[i] * 4 / 1024 > 1024 ? std::to_string(numSamples[i] * 4 / 1024 / 1024) + " MB" : std::to_string(numSamples[i] * 4 / 1024) + " KB") << "," << throughputs[i] << "\n";
        }
    }

    // close file and graph
    ResultFile.close();
    mygraph.end();

    if (ret != adf::ok)
    {
        printf("End failed\n");
        return -1;
    }

    return 0;
}
