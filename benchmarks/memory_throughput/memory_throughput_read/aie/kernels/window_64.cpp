#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "window_64.h"

/*
  Kernel to measure off chip memory read bandwidth using windows
  This kernel is set up to work with 64 bit width PLIO connections
  (this is the reason why we writeincr out twice, then we have 64 bit)
*/
#define VECTOR_LEN 8

void kernel_window_64(input_window<int> *in, output_window<int> *out)
{
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Started kernel window 64\n");
#endif

  v8int32 xV = null_v8int32();
  v8int32 zV = null_v8int32();

  const unsigned LSIZE = (NUM_SAMPLES / VECTOR_LEN);
  for (unsigned i = 0; i < LSIZE; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_readincr(in, xV);
      zV = operator+(xV, zV);
    }

  int z = 0;
  for (unsigned i = 0; i < VECTOR_LEN; i++)
  {
    z += ext_elem(zV, i);
  }
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Z: %d\n", z);
#endif

  window_writeincr(out, z);
  window_writeincr(out, 0);
}