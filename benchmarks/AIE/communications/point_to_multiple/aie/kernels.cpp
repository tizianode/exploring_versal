/* A simple kernel
 */
#include <adf.h>
#include "kernels.h"

/**
  Sender kernel: sends "1"s to all of the receivers
*/

void sender(output_window<int32> *out /*, uint32 size*/)
{
  const uint32_t length = WINDOW_SIZE / sizeof(int32_t);
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Sender, sending %u elements\n", length);
#endif
  for (unsigned i = 0; i < length; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_writeincr(out, 1);
    }
#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Sender, completed\n");
#endif
}

/**
  Receiving kernel: receives all the data
*/
void receiver(input_window<int32> *in)
{
  const uint32_t length = WINDOW_SIZE / sizeof(int32_t);

  int32 xs;

#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("Receiver, receiving %u elements\n", length);
#endif
  for (unsigned i = 0; i < length; i++)
    chess_prepare_for_pipelining
        chess_loop_range(8, )
    {
      window_readincr(in, xs);
    }

#if defined(__AIESIM__) || defined(__X86SIM__)
  printf("last element received= %d \n", xs);
#endif
}
