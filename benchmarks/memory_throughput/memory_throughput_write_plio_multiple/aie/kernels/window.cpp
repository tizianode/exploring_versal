#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
#include "window.h"

/*
  Kernel that sends NUM_SAMPLES ones (vectorised) to measure off chip memory write bandwidth using windows
*/

#define VECTOR_LEN 8

void kernel_window(output_window<int> *out)
{
  v8int32 xV = null_v8int32();
  for (int i = 0; i < VECTOR_LEN; i++)
  {
    xV = upd_elem(xV, i, 1);
  }
  for (int i = 0; i < NUM_SAMPLES; i++)
  {
    window_writeincr(out, xV);
  }
}
